# Presentaciones esLibre 2020

Para añadir las presentaciones de la edición de este año de **esLibre** tan solo tienes que subir el archivo a este mismo directorio, nombrándolo con el mismo nombre que la actividad en la [página de propuestas](https://propuestas.eslib.re/2020/).

Ahora, en el archivo
[MMXX_schedule](https://gitlab.com/eslibre/eslibre.gitlab.io/-/blob/master/_data/MMXX_schedule.yml) de la carpeta
[\_data](https://gitlab.com/eslibre/eslibre.gitlab.io/-/tree/master/_data) (que puedes encontrar en la raíz de este
repositorio) solo tienes que introducir ese mismo nombre de archivo en el campo **"file"** correspondiente a la
actividad (lo más fácil es que uses el buscador de tu editor para encontrarlo directo al buscar su título).

Una vez hecho esto, la presentación aparecerá con un icono ![](/assets/icons/save-regular.png) en el espacio correspondiente de la actividad en la página del **[programa de esLibre](https://eslib.re/2020/programa/)**. También
se podrá descargar directamente usando la dirección "https://eslib.re/2020/presentaciones/" + "nombre_archivo.pdf".

# esLibre 2020 presentations

To add presentations from this year's edition of esLibre, you just have to upload the file to this same directory, naming it with the same name as the activity in the [proposals page](https://propuestas.eslib.re/2020/).

Now, in the [MMXX_schedule](https://gitlab.com/eslibre/eslibre.gitlab.io/-/blob/master/_data/MMXX_schedule.yml) file in the [\_data](https://gitlab.com/eslibre/eslibre.gitlab.io/-/tree/master/_data) folder (which you can find at the root of this repository) you just have to enter that same file name in the **"file"** field corresponding to the activity (the easiest way is to use the search option of your text editor to find it directly when searching for its title).

Once this is done, the presentation appears with an icon ![](/assets/icons/save-regular.png) in the corresponding space of the activity in the esLibre program page. It can also be downloaded directly using the address "https://eslib.re/2020/presentaciones/" + "nombre_archivo.pdf".
