---
layout: 2024/default
section: sede
permalink: /2024/propon-sede
title: Propón sede para esLibre 2024
description: Propón sede para esLibre 2024
---

<h1 class="mt-3">
  <strong>Propón sede para es<span class="red">Libre</span> 2024</strong>
</h1>

<h5 style="text-align: center; padding-bottom: 0.5em">
Hace nada que celebramos la edición más multitudinaria de <strong>es<span class="red">Libre</span></strong> hasta la fecha, así que después de coger un poco de aire toca volver a comenzar a planificar, porque ser un evento de la comunidad para la comunidad es más necesario que nunca.
</h5>

Por quien todavía no lo sepa, **es<span class="red">Libre</span>  es un encuentro de personas interesadas en la cultura libre y las tecnologías libres enfocado a compartir nuestras experiencias alrededor de las mismas**, teniendo la intención de cada edición sea en una punto distinto para acercar todos los temas relacionados y sus comunidades a todas las regiones del país. Precisamente para ese objetivo necesitamos contar con la colaboración de comunidades o grupos locales que nos ayuden en esta labor con la idea de realizar una organización compartida: por ello ahora te pedimos que nos ayudes a organizar **es<span class="red">Libre</span> 2024**.  

### Equipo organizador

En **es<span class="red">Libre</span>** no existe una _cúpula_ dirigente como tal, pero sí que existe un grupo coordinador _general_ que son las personas que estuvieron relacionadas con la organización en pasadas ediciones y que han presentado su interés en seguir participando activamente en las cuestiones organizativas. En pasadas las ediciones, **[LibreLabGRX](https://librelabgrx.cc), [OfiLibre URJC](https://ofilibre.gitlab.io/), [LibreLabUCM](https://www.librelabucm.org/), [GALPon](https://www.galpon.org/), [Vitalinux](https://wiki.vitalinux.educa.aragon.es) y [migasfree](http://www.migasfree.org/)** se han encargado de las tareas de comunidades anfitrionas locales, contando con la ayuda de otros grupos como **[Interferencias](https://jasyp.interferencias.tech/) o [Wikimedia España](https://www.wikimedia.es/) entre otros**.

La idea es que aunque las **cuestiones organizativas y la producción del evento en general sea una tarea compartida** con todas las personas que se mueven con el interés de darle forma a esta comunidad, **haya un grupo o comunidad anfitriona local en cada edición que facilite el realizar el evento en diferentes zonas geográficas** para cada una de las mismas, dando así la posibilidad de tejer redes entre personas y comunidades en espacios comunes que quizás no tengan un contacto directo por pertenecer a ámbitos culturales, profesionales o regionales distantes entre si.

Para que el proceso sea tan transparente como el resto de decisiones relacionados con el evento, todas las propuestas serán públicas y accesibles en todo momento desde [este repositorio](https://gitlab.com/eslibre/coord/-/tree/main/propuestas/2024). Por eso mismo agradeceríamos a todas las propuestas recibidas expliquen resumidamente quienes son, si tienen experiencia previa en la organización de actividades de esta misma temática y un poco la motivación a la hora de organizar un evento de este tipo.

### Formato del congreso

**Esta sería la sexta edición hace que tengamos ya un tipo de esquema base bien definido para el congreso, pero igualmente abierto a cualquier tipo de propuesta que ayude a que sea más participativo e inclusivo**. El esquema base que se ha venido desarrollando sería algo del estilo:

-   Sesiones plenarias de apertura y cierre del congreso con todos los asistentes, participantes y organizadores
-   Una serie de pistas (que podrían ser temáticas o generales) en las que se llevarían a cabo las propuestas de actividad recibidas
-   Salas o espacios abiertos organizadas por diferentes comunidades, comprometiéndose estas a coordinarse con la organización local para intentar que logísticamente todas las actividades sean lo más homogéneas posibles (pero comprendiendo que en todo momento se respetara su criterio para las mismas)
-   Actividades sociales que también ayuden al intercambio de ideas y la socialización en entorno más distendido
-   Se puede tomar como ejemplo [el programa final de la última edición](/2023/horario/) para hacerse una idea

Aunque el objetivo es hacer que el congreso sea principalmente presencial también es deseable que exista la posibilidad de tener seguimiento online durante su desarrollo, ya que es una gran ventaja poder conectarse desde cualquier sitio para seguir el congreso y esto nos ha permitido contar con la participación de personas de varios países e incluso otros continentes.

### Acciones para mejorar la diversidad en la participación

**La organización [se compromete](/conducta) a crear un ambiente seguro, no tóxico y diverso**, de modo que nos encantaría dar voz a ponentes de las varias comunidades minoritarias que hagan uso o desarrollen software, hardware o cultura libre. Somos conscientes de que hay muchas personas que usan y crean tecnologías libre y apoyan activamente la divulgación de la cultura libre, y muchas veces no son escuchadas, queremos cambiar eso: ofrecemos nuestro micrófono y espacio para que se hable desde estas comunidades.

Desde la organización queremos aprender y crear puentes, para que la tecnología sea (al fin) un herramientas de apoyo y colaboración: buscamos un futuro colaborativo, multidisciplinar y libre.

También buscamos que las diferentes comunidades nos hagan saber lo que quieran contar desde el desarrollo o la experiencia usando software libre, por eso también nos gustaría conocer iniciativas en esta línea de acción que podrían proponer desde el equipo de organización local. Algunas de estas líneas de las que trabajamos desde la organización general:

-   La diversidad no debe centrarse únicamente en el género, por lo que se busca fomentar la participación evitando producir sentimiento de exclusión que puedan sentir personas debido a su origen étnico, religión, identidad de género, edad, habilidades físicas y experiencia u otras características de su identidad.

-   Facilitar la participación de personas que tengan responsabilidades de cuidados mediante la realización de actividades dirigidas para menores (como talleres artísticos o de robótica) y/o proveyendo de algún tipo de servicio de custodia.

### Posibles fechas

Aunque no hay nunca hay fechas perfectas, quizás los meses de **mayo-junio-julio** son la mejor opción (además que el clima suele ser también más agradable en esos meses 🙂), al igual que también tener la posibilidad de que **al menos una de los días del evento caiga en fin de semana**, para así facilitar la conciliación con las personas que trabajen y/o tengan que hacer largos desplazamientos.

Por otra parte, también debemos evitar que la fecha elegida sea en la medida de lo posible cercana a otros eventos de similar filosofía. Organizar un evento así requiere esfuerzo y dedicación, así que no deberíamos entorpecer el trabajo de otras que también intenten fomentar la cultura abierta y compartir conocimiento sobre tecnologías libres y cultura libre.

### Aspectos económicos

**es<span class="red">Libre</span> es un evento organizado voluntariamente por personas que quieren que un evento sobre software libre con asistencia gratuida sea posible**. Conocemos la dificultad de llevar a cabo un evento así sin coste alguno para el público, pero por ahora no hemos tenido la necesidad de hacerlo de otra forma gracias a la disponibilidad de uso de espacios públicos e infraestructuras de las que disponían los anteriores grupos locales.

En cualquier caso, sería necesario conocer si el propio equipo organizador local tendría acceso a algún tipo de financiación para la realización del evento o tiene intención de buscar patrocinadores que pudieran realizar aportaciones con el fin de mejorar cualquier aspecto del evento. La idea del congreso es realizarlo de forma que sea sostenible sin la necesidad de inversión económica, pero en cualquier caso, la organización general no se opondría a la presencia de patrocinadores, **siendo la única restricción que en caso de ser cualquier empresa, se deberá valorar previamente y siempre teniendo en consideración que la principal actividad de la misma no podrá chocar frontalmente con la filosofía del evento.**

Si existiera la posibilidad de cubrir este punto, comentar en qué se invertiría ese presupuesto: café y/o comida, packs de bienvenida (camisetas, acreditaciones, recuerdo local...), cartelería y medios físicos de difusión, grabación y retransmisión de las actividades...

### Facilidades disponibles

**Algunos aspectos para realizar el evento son imprescindibles, como disponer de un espacio físico asegurado**, pero hay varios aspectos que también se pueden tener en cuenta a la hora de mejorar el evento en general como puede ser disponibilidad de comida o cercanía para tomar café, posibilidad en conseguir descuentos para trasporte o la existencia de alojamientos a precios razonables cercanos a la sede del evento.

Teniendo en cuenta lo que se comentaba de la idoneidad de un modelo mixto para el congreso, también sería interesante conocer si dispondrían de medios para realizarlo: conexión a internet estable y de una velocidad aceptable, ordenados y demás equipo disponible para su uso durantes las jornadas, personas dentro de la organización local con capacidad de gestionar estos elementos...

### Planificación y uso del espacio físico

Para facilitar la planificación también es importante conocer una serie de detalles sobre el espacio físico donde se desarrollará el congreso, cualquier detalle siempre es bienvenido, pero como mínimo sería necesario conocer los siguientes aspectos:

-   Espacios disponibles, así como la facilidad de acceso, en la propia sede para las actividades (número de salas disponibles, capacidad de la mismas, salón para actos de apertura y clausura). Se recomienda que las salas tengan al menos una capacidad de 30 personas, según datos de pasadas ediciones. Pero esto no es un requisito de cara a las propuestas.
-   Medios de transporte disponibles para llegar al lugar de la sede y facilidad para llegar a la propia sede: carretera, autobús, tren, avión...
-   Si tendría espacio para alguna forma de conciliación de familias asistentes al congreso (talleres de robótica, actividades lúdicas al aire libre...)
-   Si se cuenta con el apoyo de alguna institución
-   Planes para actividades comunitarias fuera del horario del congreso (comida/cena de la comunidad, ruta turística/culturar, salida nocturna...)
-   Valoración post-evento

Para quien aproveche para viajar, seguramente también agradezca un pequeño resumen sobre la localidad que daría sede a este evento, tanto algunas cosas que la hagan característica como aspectos que puedan hacerla interesante e incluso puedan estar relacionadas con el evento (como espacios culturales en la provincia), en definitiva, cosas que podrían resultar especialmente llamativos para las personas que nunca hayan estado ahí. En el mismo caso, si la propia sede del congreso tiene alguna historia o valor cultural relevante 😎.

### Presentación de propuestas

Todas las propuestas se presentarán desde [este formulario](/2024/enviar-propuesta) y una vez recibidas quedarán accesibles desde [el mismo repositorio](https://gitlab.com/eslibre/coord/-/tree/main/propuestas/2024) que indicábamos antes.

El proceso será transparente en todo momento, y presentar las propuestas así es una _mera formalidad_ simplemente para simplificar el proceso, cualquier información se podrá ampliar o modificar en todo momento mientras que el plazo siga abierto, e igualmente si pensamos que falta alguna información importante nos pondríamos en contacto para informaros de ello.

El último día para presentar propuestas o modificarlas será el **24 de julio**. La selección de propuestas presentadas se someterá a votación de la comunidad en los días consecutivos a disponer de la lista definitiva, para que la decisión que nos permita elegir el lugar más idóneo para el transcurso de este evento en las mejores condiciones posibles también sea algo colaborativo.

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;">El plazo está abierto hasta el <strong><span class="red">24 de julio</span></strong> y puedes enviar tu propuesta desde <strong><a href="/2024/enviar-propuesta" target="_blank">este formulario</a>:<br> ¡propón tu sede para es<span class="red">Libre</span> 2024!</strong></p>

### Contacto

Para cualquier duda, cuestión o sugerencias podéis contactarnos por correo o cualquier de nuestras redes:

-   Correo electrónico: <mailto:hola@eslib.re>
-   Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:http://matrix.org)
-   Telegram: [@esLibre](https://t.me/esLibre)
-   Mastodon: [@eslibre@floss.social ](https://floss.social/@eslibre)
-   Twitter: [@esLibre\_](https://twitter.com/esLibre_)
