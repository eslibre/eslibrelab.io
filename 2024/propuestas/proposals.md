---
layout: 2024/default
section: propuestas
permalink: /2024/proposals/
title: Submission of proposals for esLibre 2024
description: We are interested to know what you do, send us your proposals!
---

<h1 class="mt-3 mb-3">Submission of proposals for <strong>es<span class="red">Libre</span></strong> 2024</h1>

<p align="center">
  <cite style="color: black;">Puedes encontrar una versión en español de este texto <a href="/2024/propuestas">aquí</a>.</cite>
</p>

<p>
  <cite style="color: grey;">Do you know someone who might want to speak at the congress? Send this page to anyone you like, we'd really appreciate it if you could help us reach more people.</cite>
</p>

<div style="width:100%">
    <div style="display: table; margin: 0 auto; width: 38%; background: ghostwhite; font-size: 20px; padding: 10px; margin-bottom: 15px; border: 1px solid lightgray;">
      • <a href="#tematica">Theme</a>
      <br>
      • <a href="#tipos">Types of proposals</a>
      <br>
      • <a href="#como">I am interested in giving a talk but... how?</a>
      <br>
      • <a href="#diversidad">Our commitment to diversity</a>
      <br>
      • <a href="#fechas">Dates</a>
      <br>
      • <a href="#enviar">How to submit your proposal</a>
    </div>
</div>

**es<span class="red">Libre</span>** is a meeting of people interested in the dissemination of libre culture and libre technologies, both in the form of libre software and libre hardware, as well as the communities that make this possible. The next edition will also be held in person (although there will be the option to follow it online) on **Friday 24 and Saturday 25 May**, hosted on this occasion by the **[GNU/Linux València](https://gnulinuxvalencia.org/)** community.

At **es<span class="red">Libre</span>** we try to hold an annual event where everyone is invited to participate, you just need to have something to say about the world of libre technologies and libre culture. We are interested in people of all ages, experiences and profiles whether they are technical or users. We are eager to listen and learn from everyone and our main goal is to reach out to communities of all kinds.

<h3 class="mt-3 mb-3" id="tematica">Theme</h3>

**Libre software**, **libre hardware** and **libre culture** are the central theme of **es<span class="red">Libre</span>**, but what we really want is to achieve an event where we can find both technical activities and others of a more informative level, or even that give rise to debates to share different points of view on aspects of the world of libre knowledge. This can include comments on publishing licenses, applications and development of different technologies, interests and concerns that different communities want to present, or the exploration and dissemination of art and humanities.

In general terms, _topics directly related to works (software, hardware, culture, etc.) that are distributed with [libre licenses](https://en.wikipedia.org/wiki/Free-software_license)_; although it doesn't have to be something strictly technological, any proposal that leans more towards the more purely informative sphere, but that is understood to meet the [definition of libre cultural work](https://freedomdefined.org/Definition/En) would also fit perfectly: **Do you have something in mind? Come on!**

Although a lot of libre software (and libre works in general) is distributed commercially without this presenting a problem, we want to keep **es<span class="red">Libre</span>** as a neutral forum with respect to commercial interests and other topics that we consider "noxious". <u>In general, we will not accept talks, workshops or any other kind of activity that specifically promotes a commercial offer or topics related to cryptocurrencies</u>.

<h3 class="mt-3 mb-3" id="tipos">Types of proposals</h3>

There are several types of activities you can propose (but not only):

-   **Talks**. Typical presentation that can be about 30 minutes long or in lightning format of about 10-15 minutes (which can be ideal to present that project you have in mind or that you have never presented yet but you would like to hear opinions 😉). The organising team may propose changes to the format of some of the talks, for organisational or content reasons. It may also, exceptionally, propose a longer format for some of the talks.

-   **Workshops**. A somewhat more practical exhibition that can be in many formats, from demonstrations where the people attending can follow a sort of "guided walk" through the theme of the workshop, to introductory sessions on a technology where they can experiment with it; in general, any practical format that you might consider interesting for making something known.

-   **Devrooms**. You can also propose the programme of a devroom, which would run in parallel with the rest of the conference activities. Usually a room will be organised by a specific community and will focus on a specific theme. For example, devrooms presented in other years:

    -   <p style="margin-bottom: 0">Libre Software at the University</p>
    -   <p style="margin-bottom: 0">Dissemination of libre culture</p>
    -   <p style="margin-bottom: 0">Privacy and digital rights</p>
    -   <p style="margin-bottom: 0">Technologies for the promotion of autochthonous languages</p>
    -   <p>Development in Perl/Raku, GNOME, functional programming...</p>

-   **Tables/exhibitors**. If you belong to any community or develop any activity related to **libre software**, **libre hardware** or **libre culture** and you would like to have a space to talk about it in a more informal way with people passing by, we will set up an area with tables where you can also leave things like stickers, pamphlets, badges or similar to make it known.

-   **Miscellaneous space**. Knowledge is not only shared in the form of talks and workshops, so you may also want to participate in other ways that do not involve these formats, here are some other types of proposals that you could send to participate in **es<span class="red">Libre</span>** in a more libre way:

    -   **Articles**. We will have a section on the website where you can display publications that you would like to share and that you would like people to know about, and you can also establish a means of contact to ask questions or make simple comments if you wish.

    -   **Posters**. Similar to the previous one, but based on a physical medium that could be exhibited during the days of the congress. This could give rise to conversations about different works in the event of finding other people interested in the same subject.

    -   **Project board**. If there is a libre software project in any field that you would like to be reported on, but you prefer to do it in a more informal way or without having to take the spotlight as in the previous types of activities, you can also propose it and we will put the means to gather people who may be interested in the same project.

    -   **Other formats**. Use your imagination! Propose other formats, we are keen to explore other ways of sharing knowledge 🐧.

<h3 class="mt-3 mb-3" id="how">I am interested in giving a talk but... how?</h3>

You've never given a talk before but you want to make your debut? At this event we would love to accompany you on that adventure. We are aware that giving a talk can be a major challenge and that's why we want to give you a series of guidelines to help you present here (or anywhere else). Here's a list of resources for presenting without using a powerpoint (🙃): [Markdown](https://www.markdownguide.org/basic-syntax/), [LaTeX](https://en.wikibooks.org/wiki/LaTeX), [Marp](https://gist.github.com/yhatt/a7d33a306a87ff634df7bb96aab058b5), [Pandoc](https://pandoc.org/getting-started.html)...

-   <p style="margin-bottom: 0">The people at TED have created a <a href="https://www.ted.com/playlists/574/how_to_make_a_great_presentation" target="_blank">list of videos</a> that teach you how to give presentations.</p>
-   <p style="margin-bottom: 0">Don't worry, you can have a cheat sheet and no one will notice (😉).</p>
-   <p style="margin-bottom: 0">To make it easier for listeners to follow you, create a table of contents with the ideas you want to talk about and follow that order during the talk.</p>
-   <p style="margin-bottom: 0">End with a summary that leaves the people who are listening to you with an idea to take back to their lives, so you can create a much more interesting impact.</p>

Again, these are all suggestions. Create your presentation the way you want and if you need further guidance and help don't hesitate to ask [the organising team](mailto:propuestas@eslib.re) for it. For inspiration, here is a list of online talks you can watch right now:

-   <p style="margin-bottom: 0"><a href="https://www.youtube.com/watch?v=ht6-jX8YF38" target="_blank">Miriam González: Sistema de diseño para dummies (TotoConf)</a></p>
-   <p style="margin-bottom: 0"><a href="https://www.youtube.com/watch?v=VGfoq5WO0Kc" target="_blank">Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)</a></p>
-   <p style="margin-bottom: 0"><a href="https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6" target="_blank">Erika Heidi: The art of programming (Codeland)</a></p>
-   <p style="margin-bottom: 0"><a href="https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13">Joe Karlsson: An introduction to IoT</a></p>

There are a lot of interesting libre projects out there to participate in, or use. You can start by [looking for a libre operating system](https://distrowatch.com/) that suits your personality, read some [libre web development projects on a libre blog](https://dev.to/ben/devto-is-now-open-source-5n1) or [libre-metasoftware on this one](http://www.elbinario.net/). You can also explore the different libre social networks in the [Fediverse](https://fediverse.party/) such as [Mastodon](https://joinmastodon.org/). Take a look at these projects and if you like them, come to the event to learn about many more.

<h3 class="mt-3 mb-3" id="diversidad">Our commitment to diversity</h3>

**The organising team [is committed](/conduct) to creating a safe, non-toxic and diverse environment**, so we would love to give a voice to speakers from the various minority communities that use or develop libre software, hardware or culture. We are aware that there are many people who use and create libre technologies and actively support the dissemination of libre culture, and often they are not heard, and we want to change that: we offer our microphone and space for these communities to speak.

From the organising team we want to learn and create bridges, so that technology is (finally) a tool for support and collaboration: we are looking for a collaborative, multidisciplinary and libre future.

We are also looking for the different communities to let us know what they want to tell us about their development or experience using libre software, so we would also like to hear about initiatives in this line of action that could be proposed by the local organising team. Some of these lines of action that we are working on from the general organising team:

-   Establish active mechanisms that can be easily called upon to ensure that the [code of conduct](/conduct) is promptly enforced in the event of any alerted behaviour that breaches it, with no exceptions either for speakers, organising team, attendees or  participants of **es<span class="red">Libre</span>. You can find all this in detail on the code of conduct page itself**.

-   Diversity should not only focus on gender, so we want to encourage different types of participation that try to diminish the feeling of exclusion that people may feel regardless of any aspect of their human condition: ethnicity, religion, gender identity, age, physical abilities and experience or other characteristics of their identity.

-   We want to facilitate the participation of people who have caring responsibilities, which can be a barrier to attendance and submissions for both men and women, but the responsibility often falls largely on women. **So this year we will also have a child care service that you can request from [this page](/2024/caring).**

-   Actively communicate in a diverse and inclusive way through different communication channels, trying to ensure that all material provided by the organisation is in formats that are easily readable by people with reading difficulties.

<h3 class="mt-3 mb-3" id="fechas">Dates</h3>

-   <p style="margin-bottom: 0"> ̶1̶s̶t̶ ̶g̶e̶n̶e̶r̶a̶l̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶ ̶s̶u̶b̶m̶i̶s̶s̶i̶o̶n̶ ̶p̶e̶r̶i̶o̶d̶:̶ ̶N̶o̶v̶e̶m̶b̶e̶r̶ ̶2̶8̶ ̶-̶ ̶J̶a̶n̶u̶a̶r̶y̶ ̶2̶8̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>
-   <p style="margin-bottom: 0">Publication of general proposals accepted 1<sup>st</sup> period: <strong>January 31st</strong>  <strong><a href="https://propuestas.eslib.re/2024/" target="_blank">[LINK]</a></strong></p>
-   <p style="margin-bottom: 0"> ̶2̶n̶d̶ ̶p̶e̶r̶i̶o̶d̶ ̶f̶o̶r̶ ̶s̶u̶b̶m̶i̶s̶s̶i̶o̶n̶ ̶o̶f̶ ̶g̶e̶n̶e̶r̶a̶l̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶:̶ ̶J̶a̶n̶u̶a̶r̶y̶ ̶2̶9̶ ̶t̶o̶ ̶M̶a̶r̶c̶h̶ ̶3̶1̶</p>
-   <p>Publication of accepted general proposals 2<sup>nd</sup> period (final program): <strong>April 3</strong></p>

-   <p style="margin-bottom: 0"> ̶D̶e̶a̶d̶l̶i̶n̶e̶ ̶f̶o̶r̶ ̶s̶u̶b̶m̶i̶s̶s̶i̶o̶n̶ ̶o̶f̶ ̶d̶e̶v̶r̶o̶o̶m̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶:̶ ̶N̶o̶v̶e̶m̶b̶e̶r̶ ̶2̶8̶ ̶t̶o̶ ̶F̶e̶b̶r̶u̶a̶r̶y̶ ̶2̶5̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>
-   <p style="margin-bottom: 0">Publication of accepted devrooms: <strong>March 3</strong>  <strong><a href="https://propuestas.eslib.re/2024/salas/" target="_blank">[LINK]</a></strong></p>
-   <p>Deadline for submission of devroom program: <strong>March 24th</strong></p>

-   <p style="margin-bottom: 0">Publication of final schedule: <strong>April 7</strong></p>
-   <p style="margin-bottom: 0"><strong>es<span class="red">Libre</span></strong> congress: <strong>May 24 and 25</strong></p>

<p style="padding-top: 0.5em; font-size: 1.25em" align="center">
  <strong>The deadline for all dates is 23:59 Madrid time (CET).</strong><br>
    <cite style="color: black;">
      <strong><span class="red">INFO</span></strong>: Accepted proposals will be published on <a href="https://propuestas.eslib.re/2024/" target="_blank">this page</a> at the end of the relevant proposal submission periods.
    </cite>
</p>

<h3 class="mt-3 mb-3" id="enviar">How to submit your proposal</h3>

The submission of proposals is open to everyone, all proposals received can be tracked at any time to ensure **transparency and feedback**, and <u>can be submitted using the forms that you can find in the following links</u>:

-   <p style="margin-bottom: 0"> ̶P̶r̶o̶p̶o̶s̶a̶l̶s̶ ̶f̶o̶r̶ ̶t̶a̶l̶k̶s̶ ̶a̶n̶d̶ ̶w̶o̶r̶k̶s̶h̶o̶p̶s̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>
-   <p style="margin-bottom: 0"> ̶P̶r̶o̶p̶o̶s̶a̶l̶s̶ ̶f̶o̶r̶ ̶d̶e̶v̶r̶o̶o̶m̶s̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>
-   <p style="margin-bottom: 0"> ̶P̶r̶o̶p̶o̶s̶a̶l̶s̶ ̶f̶o̶r̶ ̶t̶a̶b̶l̶e̶s̶/̶e̶x̶h̶i̶b̶i̶t̶o̶r̶s̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>
-   <p> ̶M̶i̶s̶c̶e̶l̶l̶a̶n̶e̶o̶u̶s̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶&nbsp;&nbsp;<strong><span class="red">[ENDED]</span></strong></p>

All proposals for **es<span class="red">Libre</span>** submitted through these forms are registered in our **[proposal repository](https://gitlab.com/eslibre/propuestas/-/merge_requests?scope=all&state=opened&label_name[]=2024)**. If any questions or suggestions need to be clarified, this will also be done in the same place, so keep an eye on it. In any case, <u>once you register your proposal through the form, you will receive an email with the information of the registered participation and a direct link to your proposal in the repository</u>.

If you have any problems or questions, you can write to us at <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">COME AND PARTICIPATE!!!<br>👏👏👏</h3>
