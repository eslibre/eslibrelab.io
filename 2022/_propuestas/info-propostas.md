---
layout: 2022/default-gl
section: propuestas
permalink: /2022/info-propostas
title: Envío de propostas para esLibre 2022
description: Interésanos coñecer que cousas fas. Mándanos propostas!
---

<h1 class="mt-3 mb-3">Envío de propostas para <strong>es<span class="red">Libre</span></strong> 2022</h1>

<p align="center">
  <cite style="color: black;">Puedes encontrar una versión en español de este texto <a href="/2022/info-propuestas">aquí</a>.</cite>
</p>

<p align="center">
  <cite style="color: black;">You can find an English version of this text <a href="/2022/info-proposals">here</a>.</cite>
</p>

<p style="padding-top: 0.5em; font-size: 1.5em" align="center">
  <cite style="color: black;">
    <strong><span class="red">ATENCIÓN</span></strong>: Envío de propostas ampliado ata o 7 de maio.
  </cite>
</p>

<p>
  <cite style="color: grey;">Coñeces a alguén que poida querer falar no congreso? Envíalle esta páxina a quen queiras, agradecerémoscho unha chea se nos axúdas a chegar a máis xente.</cite>
</p>

O [congreso **es<span class="red">Libre</span>**](https://eslib.re) é un encontro de _persoas interesadas nas **tecnoloxías libres** e a **cultura libre**, enfocado en compartir coñecemento e experiencias ao seu redor_. A próxima edición volverá ser presencial (aínda que tamén se poderá seguir en liña) os días venres 24 e sábado 25 de xuño, sendo nesta ocasión as comunidades anfitrioas **[GALPon - Grupo de Amigos de Linux de Pontevedra](https://www.galpon.org/)** e **[AGASOL (Asociación de Empresas Galegas de Software Libre Agasol)](https://www.agasol.gal/)**.

En **es<span class="red">Libre</span>** tentamos manter un evento anual no que todo o mundo está convidado a participar, o único que necesita é ter algo que dicir sobre o mundo das tecnoloxías e cultura libre. Interésannos persoas de todas as idades, experiencias e perfís, sexan técnicas ou usuarias. Estamos a desexar escoitar e aprender de todas as persoas e o noso obxectivo principal é chegar a comunidades de todo tipo.

<h3 class="mt-3 mb-3" id="tematica">Temática</h3>

**Software libre**, **hardware libre** e **cultura libre** son a temática central de **es<span class="red">Libre</span>**, pero o que realmente queremos é conseguir un evento no que atopemos tanto actividades técnicas como outras dun nivel máis divulgativo, ou mesmo que dean pé a debates para compartir diferentes puntos de vista sobre aspectos do mundo do coñecemento libre. Isto pode comprender comentarios sobre licenzas de publicación, aplicacións e desenvolvemento de diversas tecnoloxías, intereses e inquedanzas que queiran presentar diferentes comunidades, ou a exploración e divulgación da arte libre.

En xeral, _temas relacionados directamente con obras (software, hardware, cultura, etc.) que se distribúan con [licenzas libres](https://gl.wikipedia.org/wiki/Software_libre#Licenzas)_; aínda que non ten que ser algo estritamente tecnolóxico, encaixaría tamén á perfección calquera proposta que se incline máis ao ámbito máis puramente divulgativo, pero que se entenda conforme coa [definición de obra cultural libre](https://freedomdefined.org/Definition/Gl): **Tes mentes de algo? Anímate!**

Aínda que gran parte do software libre (e obras libres en xeral ) distribúese comercialmente sen que isto supoña problema ningún, queremos manter **es<span class="red">Libre</span>** como un foro neutral con respecto aos intereses comerciais. <u>En xeral, non se aceptarán relatorios, obradoiros nin ningún outro tipo de actividade que promovan especificamente unha oferta comercial</u>.

<h3 class="mt-3 mb-3" id="tipos">Tipos de propostas</h3>

Hai varios tipos de actividades que podes propor (pero non só):

-   **Relatorios**. Presentación típica que pode ter uns 30 minutos aproximadamente de duración ou en formato lóstrego (sobre uns 10-15 minutos). A organización poderá propor cambios de formato a algúns dos relatorios, por motivos organizativos ou de contidos. Tamén poderá, excepcionalmente, propor a algun relatorio nun formato máis longo.

-   **Obradoiros**. Exposición algo máis práctica que pode ter moitos formatos, dende demostracións onde as persoas que asistan poidan seguir unha especie de «paseo guiado» pola temática do obradoiro, ata sesións de iniciación a unha tecnoloxía onde poder experimentar con ela; en xeral, calquera formato práctico que poidas considerar que sexa interesante para dar a coñecer algo.

-   **Salas (devroom)**. Tamén podes propor o programa dunha sala, que se realizaría en paralelo co resto de actividades do congreso. Normalmente unha sala estará organizada por unha comunidade específica e estará centrada nun tema específico. Por exemplo, salas presentadas outros anos:
    -   Software Libre na Universidade
    -   Divulgación da cultura libre
    -   Privacidade e dereitos dixitais
    -   Tecnoloxías para o fomento da lingua andaluza
    -   Desenvolvemento en Perl/Raku, GNOME, programación funcional...
        <br><br>
-   **Espazo de misceláneas**: O coñecemento non é compartido só en forma de relatorios e obradoiros, polo que é posíbel que tamén queiras participar doutra forma que non implique estes formatos, deixámosche aquí outros tipos de propostas que poderías enviar para participar en **es<span class="red">Libre</span>** dun xeito máis libre:

    -   **Artigos**. Teremos un apartado na web onde expor publicacións realizadas que queiras compartir e que che gustaría que a xente puidese coñecer, podendo ademais estabelecer algún medio de contacto para formular dúbidas ou simples comentarios se así o desexas.

    -   **Pósters**. Semellante ao anterior, pero baseandose nun medio físico que se podería expor durante os días do congreso. O que pode dar lugar a conversas sobre diferentes traballos no caso de atopar outras persoas interesadas no mesmo tema.

    -   **Taboleiro de proxectos**. Se hai un proxecto de software libre de calquera campo sobre o que che gustaría que se falase, pero prefires que sexa dun xeito máis informal ou sen ter que asumir o foco de atención como nos tipos de actividades anteriores, tamén podes propoñelo e poremos os medios para reunir xente que poida estar interesada no mesmo proxecto.

    -   **Outros formatos**. Usa o teu maxín! Propón outros formatos, temos moito interese en explorar outras formas de compartir coñecemento 🐧.

<h3 class="mt-3 mb-3" id="como">Interésame impartir unha charla mais... como?</h3>

Nunca impartiches un relatorio pero queres debutar? Neste evento encantaríanos acompañarte nesa aventura. Somos conscientes de que impartir un relatorio pode ser un reto importante e por iso queremos darvos unha serie de pautas que vos axuden a expor aquí (ou onde sexa). Deixámosvos unha lista de recursos para facer presentacións sen usar un «powerpoint» (🙃): [Markdown](https://markdown.es/sintaxis-markdown/), [LaTeX](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Texto_completo), [Marp](https://www.genbeta.com/herramientas/marp-herramienta-que-nos-permite-crear-presentaciones-modo-texto-usando-markdown), [Pandoc](https://ondiz.github.io/cursoLatex/Contenido/15.Pandoc.html)...

-   A xente de TED creou unha [lista de vídeos](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) que ensinan a facer presentacións.
-   Non escribas moito texto na presentación, fai unha lista de puntos clave e desenvólveos falando. Non te preocupes, podes ter unha folla de trucos e ninguén se decatará (😉), mais procura non ler todo o tempo ou non quedará igual de fluído. Déixate levar! Pensa que estas nunha conversa con alguén próximo.
-   Para que as e os asistentes que te escoiten poidan seguirte mellor, crea un índice expoñendo as ideas das que queres falar e segue esa orde durante o relatorio.
-   Remata cun resumo que deixe as persoas que che están escoitando cunha idea que levar consigo, así poderás crear un impacto moito máis interesante.

Unha vez máis, todo isto son suxestións. Crea a túa presentación como queiras e se necesitas máis orientación ou axuda non dubides en pedírllelas [a organización](mailto:propuestas@eslib.re). Para inspirarte, deixámosche unha lista de relatorios en liña que podes ver agora mesmo:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

Aí fóra hai unha manchea de proxectos interesantes libres cos que participar, ou utilizar. Podes comezar [buscando un sistema operativo libre](https://distrowatch.com/) que vaia acorde coa túa personalidade, ler algúns proxectos de desenvolvemento web nun [blog libre](https://dev.to/) ou de metasoftware-libre [nestoutro](http://www.elbinario.net/). Tamén podes explorar as diferentes redes sociais libres do [Fediverso](https://fediverse.party/) como [Mastodon](https://joinmastodon.org/). Fai un percorrido por estes proxectos e se che gustan, achégate ao evento para coñecer moitos máis.

<h3 class="mt-3 mb-3" id="diversidad">O noso compromiso pola diversidade</h3>

**A organización [comprométese](/conduta) a crear un ambiente seguro, non tóxico e diverso**,  de modo que nos encantaría dar voz a falantes das diversas comunidades minoritarias que utilicen ou desenvolvan software, hardware ou cultura libre. Somos conscientes de que hai moitas persoas que usan e crean tecnoloxías libre e apoian activamente a divulgación da cultura libre, e moitas veces non son escoitadas, queremos cambiar iso: ofrecemos os nosos micrófonos e espazos para que estas comunidades falen.

Dende a organización queremos aprender e crear pontes, para que a tecnoloxía sexa (ao fin) unha ferramenta de apoio e colaboración: buscamos un futuro colaborativo, multidisciplinar e libre.

Buscamos tamén que as diferentes comunidades nos fagan saber o que queren contar dende o desenvolvemento ou a experiencia de uso do software libre, por iso tamén nos gustaría coñecer iniciativas nesta liña de acción que poderían ser propostas dende o equipo de organización local. Algunhas destas liñas das que traballamos dende a organización xeral:

-   Estabelecer mecanismos activos aos que poidan ser empregados facilmente para garantir que o [código de conduta](/conduta) se cumpra rapidamente ante calquera comportamento alertado que o incumpra sen excepcións nin para relatores, organización, asistentes ou participantes de <strong>es<span class="red">Libre</span></strong>. **Podes atopar todo isto detallado na propia páxina do [código de conduta](/conduta)**.
-   A diversidade non debe centrarse unicamente no xénero, por iso fomentamos que haxa diferentes tipos de participación que tenten diminuír o sentimento de exclusión que poidan sentir as persoas sen importar calquera aspecto da súa condición humana: orixe étnica, relixión, identidade de xénero, idade, habilidades físicas e experiencia ou outras características da súa identidade.
-   Queremos facilitar a participación de persoas que teñan responsabilidades de coidados, algo que aínda que pode ser unha barreira para a asistencia e envío de propostas tanto para homes como para mulleres, a responsabilidade a miúdo recae en gran medida nas mulleres. **Toda a información sobre estas facilidades iranse engadindo [nesta páxina](/2022/coidados) segundo se vaian concretando**.
-   Realizar de forma activa unha comunicación diversa e inclusiva mediante diferentes canles de comunicación, tentando que todo o material fornecido pola organización estea escrito en tipos de letra e cores que sexan facilmente lexíbeis por persoas con algún tipo de dificultade de lectura ou aprendizaxe, incidindo en elementos como o tamaño de letra e o contraste coas cores de fondo que axuden ás persoas con problemas visuais.

Por outra banda, haberá un fondo de axudas económicas creado para facilitar a asistencia das persoas que formen parte de colectivos menos representados. Podes atopar toda a información sobre esta axuda [nesta páxina](/2022/bolsa-viaxe/), mais se tes calquera dúbida ao respecto, tamén podes escribirnos a <mailto:info@eslib.re>.

<h3 class="mt-3 mb-3" id="fechas">Datas</h3>

-   Data límite **estendida** para enviar propostas: **7 de maio**
-   Publicación do programa coas propostas aceptadas: **9 de maio**
-   Data límite para enviar o programa das salas aceptadas: **22 de maio**
-   Celebración do congreso **es<span class="red">Libre</span>**: **24 e 25 de xuño**

<p style="padding-top: 0.5em; font-size: 1.0em" align="center">
  <strong>O límite de todas as datas son ás 23:59 hora en Madrid (CET).</strong><br>
    <cite style="color: black;">
      <strong><span class="red">INFO</span></strong>: <a href="https://propuestas.eslib.re/2022/" target="_blank">Nesta páxina</a> iranse publicando as actividades que foron aceptadas durante o primeiro período de envío de propostas.
    </cite>
</p>

<h3 class="mt-3 mb-3" id="enviar">Como enviar a túa proposta</h3>

O envío de propostas está aberta a todo o mundo, todas as propostas recibidas poderán seguirse en todo momento para garantir a **transparencia e a realimentación**, e <u>poderanse enviar mediante os formularios que atoparás nas seguintes ligazóns</u>:

-   [Propostas de relatorios e obradoiros](/2022/enviar-propostas)
-   [Propostas de salas](/2022/enviar-salas-gl)
-   [Propostas misceláneas](/2022/espazo-misc)

Todas as propostas para <strong>es<span class="red">Libre</span></strong> que se envíen mediante eses formularios quedan rexistradas no noso **[repositorio de propostas](https://gitlab.com/eslibre/propuestas/-/merge_requests)**. Se fose preciso clarexar calquera dúbida ou suxestión tamén se faría nese mesmo lugar, polo que non o perdas de vista. En calquera caso, <u>unha vez que rexistres a túa proposta mediante o formulario, chegarache un correo electrónico coa información da participación rexistrada e a ligazón directa á túa proposta dentro do repositorio</u>.

Se tes algún problema ou dúbida de calquera tipo, podes escribirnos a <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">ANÍMATE A PARTICIPAR!!!<br>👏👏👏</h3>
