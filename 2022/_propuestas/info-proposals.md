---
layout: 2022/default-en
section: propuestas
permalink: /2022/info-proposals
title: Submission of proposals for esLibre 2022
description: We are interested in knowing what you do. Send us proposals!
---

<h1 class="mt-3 mb-3">Submission of proposals for <strong>es<span class="red">Libre</span></strong> 2022</h1>

<p align="center">
  <cite style="color: black;">Podes atopar unha versión en galego deste texto
<a href="/2022/info-propostas">aquí</a>.</cite>
</p>

<p align="center">
  <cite style="color: black;">Puedes encontrar una versión en español de este texto <a href="/2022/info-propuestas">aquí</a>.</cite>
</p>

<p style="padding-top: 0.5em; font-size: 1.5em" align="center">
  <cite style="color: black;">
    <strong><span class="red">ATTENTION</span></strong>: Submission of proposals extended until May 7.
  </cite>
</p>

<p>
  <cite style="color: grey;">Do you know someone who might want to speak at the congress? Send this page to whoever you want, we will really appreciate it if you help us reach more people.</cite>
</p>

The [**es<span class="red">Libre</span>** congress](https://eslib.re) is a meeting of _people interested in **libre technologies** and **libre culture**, focused on sharing knowledge and experiences around them_. The next edition will once again be face-to-face (although it can also be followed online) on Friday 24 and Saturday 25 June, being on this occasion the host communities **[GALPon - Grupo de Amigos de Linux de Pontevedra ](https://www.galpon.org/)** and **[AGASOL (Asociación de Empresas Galegas de Software Libre Agasol)](https://www.agasol.gal/)**.

At **es<span class="red">Libre</span>** we try to maintain an annual event where everyone is invited to participate, the only thing that needs to have something to say about the world of libre technology and libre culture. We are interested in people of all ages, experiences and profiles, whether they are technicians or users. We are looking forward to hearing and learning from everyone and our main goal is to reach communities of all kinds.

<h3 class="mt-3 mb-3" id="tematica">Theme</h3>

**Libre software**, **libre hardware** and **libre culture** are the central topic of **es<span class="red">Libre</span>**, but what we really want is to achieve an event where we find both technical activities and others of a more informative level, or even that give rise to debates to share different points of view on aspects of the world of libre knowledge. This can include comments on publication licenses, applications and development of various technologies, interests and concerns that different communities want to present, or the exploration and dissemination of libre art.

In general terms, _topics directly related to works (software, hardware, culture, etc.) that are distributed with [libre licenses](https://en.wikipedia.org/wiki/Free-software_license)_; although it does not have to be something strictly technological, any proposal that is more inclined to the more purely informative field, but that is understood to meet the [definition of libre cultural work](https://freedomdefined.org/Definition/En) would also fit perfectly: **Do you have something in mind? Cheer up!**

Although much libre software (and libre works in general) is distributed commercially without this presenting any problem, we want to keep **es<span class="red">Libre</span>** as a neutral forum with respect to commercial interests. In general, talks, workshops or any other type of activity that specifically promotes a commercial offer will not be accepted.

<h3 class="mt-3 mb-3" id="tipos">Types of proposals</h3>

There are several types of activities that you can propose (but not only):

-   **Talks**. Typical presentation that can be approximately 30 minutes long or in a lightning format (about 10-15 minutes). The organization may propose format changes to any of the talks, for organizational or content reasons. You may also, exceptionally, propose a longer format for a talk.

-   **Workshops**. A somewhat more practical exhibition that can be in many formats, from demonstrations where people who attend can follow a kind of "guided walk" through the theme of the workshop, to introductory sessions on a technology where they can experiment with it; in general, any practical format that you can find interesting to publicize something.

-   **Devrooms**. You can also propose the program of a devroom, which would be carried out in parallel with the rest of the activities of the congress. Usually a room will be organized by a specific community and will be focused on a specific topic. For example, rooms presented other years:
    -   Libre Software at the University
    -   Dissemination of libre culture
    -   Privacy and digital rights
    -   Technologies for the promotion of the Andalusian language
    -   Development in Perl/Raku, GNOME, functional programming...
        <br><br>
-   **Miscellaneous space**: Knowledge is not only shared in the form of talks and workshops, so you may also want to participate in another way that does not involve these formats, we leave you here other types of proposals that you could send to participate in esLibre in a way more free:

    -   **Articles**. We will have a section on the web where you can expose publications made that you want to share and that you would like people to know about, and you can also establish a means of contact to raise questions or simple comments if you wish.

    -   **Posters**. Similar to the previous one, but based on a physical environment that could be exhibited during the days of the congress. Which could lead to conversations about different jobs if you find other people interested in the same topic.

    -   **Project board**. If there is a libre software project in any field that you would like to hear about, but you prefer it to be done in a more informal way or without having to assume the focus of attention as in the previous types of activities, you can also propose it and we will provide the means to bring together people who may be interested in the same project.

    -   **Other formats**. Use your imagination! Propose other formats, we are very interested in exploring other ways of sharing knowledge 🐧.

<h3 class="mt-3 mb-3" id="how">I am interested in giving a talk but ... how?</h3>

Have you never given a talk but want to debut? In this event we would love to accompany you on that adventure. We are aware that giving a talk can be an important challenge and that is why we want to give you a series of guidelines that will help you to exhibit here (or wherever). We leave you a list of resources to expose without using a "powerpoint" (🙃): [Markdown](https://www.markdownguide.org/basic-syntax/), [LaTeX](https://en.wikibooks.org/wiki/LaTeX), [Marp](https://gist.github.com/yhatt/a7d33a306a87ff634df7bb96aab058b5), [Pandoc](https://pandoc.org/getting-started.html)...

-   The folks at TED have created a [video list](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) that teach you how to make a presentation.
-   Don't worry, you can have a cutlet and no one will notice (😉).
-   So that the attendees who listen to you can follow you better, create an index exposing the ideas you want to talk about and follow that order during the talk.
-   End with a summary that give the people who are listening to you an idea to take into their lives, this way you will create a much more interesting impact.

Again, these are all suggestions. Create your presentation as you want and if you need more guidance and help, do not hesitate to ask [the organization](mailto:propuestas@eslib.re) for it. To inspire you, we leave you a list of online talks that you can watch right now:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

There are a lot of cool libre projects out there to participate in, or use. You can start [looking for a libre operating system](https://distrowatch.com/) that fits your personality, read some web development projects in a [libre blog](https://dev.to/) or libre-metasoftware in [this other](http://www.elbinario.net/). You can also explore the different libre social networks of the [Fediverse](https://fediverse.party/) like [Mastodon](https://joinmastodon.org/). Take a tour of these projects and if you like them, come to the event to learn about many more.

<h3 class="mt-3 mb-3" id="diversidad">Our commitment to diversity</h3>

**The organization [is committed](/conduct) to creating a safe, non-toxic, and diverse environment**, so we'd love to give a voice to speakers from the various minority communities that use or develop libre software, hardware, or culture. We are aware that there are many people who use and create libre technologies and actively support the dissemination of libre culture, and many times they are not heard, we want to change that: we offer our microphone and space for these communities to speak.

From the organization we want to learn and create bridges, so that technology is (finally) a support and collaboration tool: we seek a collaborative, multidisciplinary and free future.

We also want the different communities to let us know what they want to tell from the development or experience using libre software, so we would also like to know initiatives in this line of action that could be proposed by the local organization team. Some of these lines that we work from the general organization:

-   Establish active mechanisms that can be easily resorted to in order to ensure that the [code of conduct](/conduct) is quickly complied with in the event of any alerted behavior that violates it, without exceptions, neither for speakers, organization, attendees or participants of <strong>es<span class="red">Libre</span></strong>. **You can find all this detailed on the [code of conduct](/conduct) page itself**.
-   Diversity should not focus solely on gender, which is why we encourage different types of participation that try to reduce the feeling of exclusion that people may feel regardless of any aspect of their human condition: ethnic origin, religion, gender identity, age , physical abilities and experience or other characteristics of your identity.
-   We want to facilitate the participation of people who have care responsibilities, something that although it can be a barrier for both men and women to attend and submit proposals, the responsibility often falls largely on women. **All the information about these facilities will be added to [this page](/2022/caring) as they are specified**.
-   Carry out actively diverse and inclusive communication through different communication channels, trying to ensure that all the material provided by the organization is written in fonts and colors that are easily readable by people with some type of reading or learning difficulty, focusing on elements such as the font size and contrast with background colors to also help visually impaired people.

<h3 class="mt-3 mb-3" id="fechas">Dates</h3>

-   **Extended** deadline to submit proposals: **May 7**
-   Publication of the program with the accepted proposals: **May 9**
-   Deadline to send the program of the accepted rooms: **May 22**
-   Celebration of the **es<span class="red">Libre</span>** congress: **June 24 and 25**

<p style="padding-top: 0.5em; font-size: 1.0em" align="center">
  <strong>The deadline for all dates is at 23:59 Madrid time (CET).</strong><br>
    <cite style="color: black;">
      <strong><span class="red">INFO</span></strong>: We will publish the activities that have been accepted during the first proposal submission period on <a href="https://propuestas.eslib.re/2022/" target="_blank">this page</a>.
    </cite>
</p>

<h3 class="mt-3 mb-3" id="enviar">How to submit your proposal</h3>

The submission of proposals is open to everyone, all received proposals can be followed at all times to guarantee **transparency and feedback**, and can be sent using the forms that you can find in the following links:

-   [Talks and workshops proposals](/2022/send-proposals)
-   [Devrooms proposals](/2022/send-devrooms)
-   [Miscellaneous proposals](/2022/misc-space)

All proposals for <strong>es<span class="red">Libre</span></strong> that are sent through these forms are registered in our **[repository of proposals](https://gitlab.com/eslibre/propuestas/-/merge_requests)**. If it were necessary to clarify any question or suggestion, it would also be done in that same place, so do not lose sight of it. In any case, <u>once you register your proposal using the form, you will receive an email with the registered participation information and a direct link to your proposal within the repository</u>.

If you have problems of any kind or any kind of doubt, you can write to us at <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">CHEER UP TO PARTICIPATE!!!<br>👏👏👏</h3>
