---
layout: 2022/default-en
section: info
permalink: /2022/rules
title: Rules for attendance at esLibre 2022
---

<h1 style="padding-bottom: 0.5em;" class="mt-3">
  Rules for attendance at <strong>es<span class="red">Libre</span></strong>
</h1>

<p align="center">
  <cite style="color: black;">Last update: April 21, 2022</cite>
</p>

In accordance with the new COVID-19 prevention regulations, the regulations indicated on this page <span style="color: red;"><strong>are SUSPENDED</strong></span>. Even so, since we are going to be in closed spaces, with quite a few people and often very close to each other, we allow ourselves to advise you, following the recommendations of the Galician administration, that you continue to use a face mask inside the rooms.

<p align="center">
  <cite style="color: black;">Last update: February 13, 2022</cite>
</p>

In view of the health situation at the time of writing these rules, the organization wishes to inform that this organization is committed to strictly observe each and every one of the rules, provisions and / or obligations determined by the state and / or regional authorities. For this reason, in the event that a situation similar to the current one is maintained, we consider it appropriate to inform of the following **obligations for ALL participants**.

> The refusal to respect any decision, in this respect, of the organization will mean the loss of the right to attend any act in this event.

- The use of face masks must be worn at all times and properly fitted (covering the nose and mouth).
- Only speakers may remove their masks at the time of their public presentation, provided that there is a minimum distance of 2 metres from the nearest audience.
- If you forget or your mask gets damaged you can go to INFORMATION and request, free of charge, a new mask.
- ALL attendees are obliged to show the QR code of their EU digital COVID Certificate together with their ID card or passport when entering the facilities and/or whenever requested to do so by a duly accredited member of the organisation.
- ALL attendees are obliged to allow their body temperature to be taken, by methods that do not contact their body, when entering the facilities and/or whenever required to do so by a duly accredited member of the organisation.
- Speakers are required to take a COVID-19 SARS-CoV-2 antigen test no more than 3 hours before the start of their public presentation.
- All the presentation rooms will be equipped with CO2 meters, at least 2 of which will be properly distributed in the auditorium.

> If you have any questions, do not hesitate to write to us at <hola@eslib.re>.
