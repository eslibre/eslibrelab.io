---
layout: 2022/default
section: info
permalink: /2022/normas-es
title: Normativa para la asistencia a esLibre 2022
---

<h1 style="padding-bottom: 0.5em;" class="mt-3">
  Normativa para la asistencia a <strong>es<span class="red">Libre</span></strong>
</h1>

<p align="center">
  <cite style="color: black;">Última actualización: 21 de abril de 2022</cite>
</p>

Conforme a la nueva normativa de prevención de la COVID-19, las normas indicadas en esta página <span style="color: red;"><strong>quedan en SUSPENSO</strong></span>. Aún así, ya que vamos a estar en espacios cerrados, con bastante gente y con frecuencia, muy próximos unos de otros, nos permitimos aconsejaros, siguiendo las recomendaciones de la administración gallega, que sigáis utilizando mascarilla cubrebocas en el interior de las salas.

<p align="center">
  <cite style="color: black;">Última actualización: 13 de febrero de 2022</cite>
</p>

A la vista de la situación sanitaria en el momento de redactar estas normas, la organización quiere informar que esta organización se compromete a observar, estrictamente, todas y cada una de las normas, disposiciones y/o obligaciones que determinen las autoridades estatales y/o autonómicas. Por esto, en caso de mantenerse una situación semejante a la actual, consideramos conveniente informar de las siguientes **obligaciones para TODA persona participante**.

> La negativa a respetar cualquier decisión, a este respecto, de la organización supondrá la perdida del derecho a asistir a cualquier acto en este evento.

- Es obligatorio el uso de mascarilla en todo momento y debidamente colocada (tapando nariz y boca).
- Solo las (personas) ponentes podrán desprenderse de su mascarilla en el momento de su exposición pública siempre que exista una distancia mínima de 2 metros con la audiencia más próxima.
- Si olvidas o se estropea tu mascarilla podrás acudir a INFORMACIÓN y solicitar, gratuítamente, una mascarilla.
- TODAS las personas asistentes están obligadas a mostrar el código QR de su Certificado COVID digital de la UE junto con su Documento/Cédula de identificación o pasaporte al acceder a las instalaciones y/o siempre que le sea requerido por algún miembro, debidamente acreditado, de la organización.
- TODAS las personas asistentes están obligados a permitir que se les tome la temperatura corporal, por métodos que no contacte con su cuerpo, al acceder a las instalaciones y/o siempre que le sea requerido por algún miembro, debidamente acreditado, de la organización.
- Las (personas) ponentes están obligadas a hacerse una prueba de antígenos COVID-19 SARS-CoV-2 no más de 3 horas antes de iniciar su exposición pública.
- Todas las salas de ponencia dispondrán de medidores de CO2, en el salón de actos se dispondrá de (al menos) 2 debidamente distribuídos.

> Si tienes cualquier duda, no dudes en escribirnos a <hola@eslib.re>.
