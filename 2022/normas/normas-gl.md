---
layout: 2022/default-gl
section: info
permalink: /2022/normas-gl
title: Normativa para a asistencia a esLibre 2022
---

<h1 style="padding-bottom: 0.5em;" class="mt-3">
  Normativa para a asistencia a <strong>es<span class="red">Libre</span></strong>
</h1>

<p align="center">
  <cite style="color: black;">Última actualización: 21 de abril de 2022</cite>
</p>

Consonte a nova normativa de prevención da COVID-19, as normas indicadas nesta páxina <span style="color: red;"><strong>quedan en SUSPENSO</strong></span>. Ainda así, xa que imos estar en espazos pechados, con bastante xente e con frecuencia, moi próximos un doutros, permitimonos aconsellaros, siguiendo las recomendaciones de la administración gallega que sigade utilizando mascaras cubrebocas no interior das salas.

<p align="center">
  <cite style="color: black;">Última actualización: 13 de febreiro de 2022</cite>
</p>

Á vista da situación sanitaria no momento de redactar estas normas, a organización quere informar que esta organización comprométese a observar, estritamente, todas e cada una das normas, disposicións e/ou obrigas que determinen as autoridades estatais e/ou autonómicas. Por isto, no caso de manterse unha situación semellante e actual, consideramos conveniente informar das seguintes **obrigas para TODA persoa participante**.

> A negativa a respectar calquera decisión, neste eido, da organización suporá a perda do dereito a asistir a calquera acto neste evento.

- É obrigatorio o uso da máscara en todo momento e correctamente colocadas (cubrindo o nariz e a boca).
- Só as (persoas) relatoras poderán tirar a súa máscara no momento da súa exposición pública sempre que exista unha distancia mínima de 2 metros coa audiencia máis próxima.
- Se esqueces ou se estraga a túa máscara poderás acudir a INFORMACIÓN e solicitar, de balde, unha máscara.
- TODAS as persoas asistentes están obrigadas a amosar o código QR da súa Certificado COVID dixital da UE xunto con o seu Documento/Cédula de identificación ou pasaporte ao acceder ás instalacións e/ou sempre que lle sexa requirido por algún membro, debidamente acreditado, da organización.
- TODAS as persoas asistentes están obrigados a permitir que se lles tome a temperatura corporal, por métodos que non contacten co seu corpo, ao acceder ás instalacións e/ou sempre que lle sexa requirido por algún membro, debidamente acreditado, da organización.
- As (persoas) relatoras están obrigadas a facerse unha proba de antíxenos COVID-19 SARS-CoV-2 non máis 3 horas antes de iniciar a súa exposición pública.
- Todas as salas de relatorios disporán de medidores de CO2, no salón de actos disporase de (polo menos) 2 debidamente distribuídos.

> Si tes calquera dúbida, non dubides en escribirnos a <hola@eslib.re>.
