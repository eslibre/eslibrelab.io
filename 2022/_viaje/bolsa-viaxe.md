---
layout: 2022/default-gl
section: acerca
permalink: /2022/bolsa-viaxe
title: Solicitude de fondos para participar en esLibre
---

<h1 class="mt-3">
  Solicitude de fondos para participar en <strong>es<span class="red">Libre</span></strong>
</h1>

Este ano, ademais de volver ter unha **edición presencial**, tamén podemos anunciar que teremos a oportunidade de axudar a financiar os gastos de viaxe dalgunhas persoas que queiran participar en **es<span class="red">Libre</span>**.

Decidimos que o obxectivo fundamental das axudas é apoiar a participación de colectivos no noso congreso, especialmente de colectivos de **xénero** (mulleres, persoas trans e minorías de xénero infrarrepresentadas en **es<span class="red">Libre</span>**), ademais de apoiar tamén a persoas con discapacidade, persoas neurodiverxentes e/ou neurodiversas e persoas con enfermidades. O obxectivo fundamental desta estratexia é crear un espazo seguro no que debater sobre o software libre, destacando realidades diversas e a súa relación coa tecnoloxía.

Tamén queremos ofrecer axuda xeral ás persoas que, pola súa situación económica, teñan problemas para cubrir os gastos de desprazamento e aloxamento durante o evento. Por exemplo, teremos especial atención á situación dos estudantes, xubilados, desempregados ou con emprego precario. Os criterios e a proporción das axudas serán personalizados para cada caso, polo que non dubides en consultalos.

<p style="text-align: center;"><strong>Para optar ás axudas para persoas en todas estas situacións, abonda con indicalo no <a href="/2022/enviar-propostas" target="_blank">formulario de presentación de propostas</a>.</strong></p>

En **es<span class="red">Libre</span>** queremos promover relatorios, obradoiros e actividades que non teñen por que ser necesariamente técnicas, senón que poden ser conversas de diversos ámbitos (humanísticos, sociais, artísticos, etc.) sobre tecnoloxía libre. Este proceso tenta conseguir unha **es<span class="red">Libre</span>** máis diversa apoiando directamente a quen queira falar de Software Libre, independentemente da súa situación ou contexto persoal. Priorizaranse as axudas aos solicitantes en función do impacto que estimamos que tería a súa proposta no programa do congreso.

Se es unha **persoa que podería optar a estas axudas** mais **non te sentes cómoda participando activamente** presentando calquera tipo de proposta, **utiliza [este formulario](/2022/form-bolsa)** para poñerte en contacto e veremos como podemos axudarche a asistir ao congreso.

<p style="text-align: center;"><strong>Se tes algunha dúbida, non dubides en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</strong></p>

<div class="media">
  <img style="display: block !important; margin-left: auto !important; margin-right: auto; !important; max-width: 60% !important; height: auto !important;" src="/2022/assets/imgs/banner_anuncio.png" alt="Logo esLibre" class="jumbotron mt-3 embed-responsive">
</div>
