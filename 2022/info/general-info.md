---
layout: 2022/default
section: info
permalink: /2022/general-info
title: About esLibre 2022
---

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edition 2022
</h1>

<p align="center">
  <cite style="color: red;">Page with information that will be updated regularly.</cite>
</p>

**es<span class="red">Libre</span>** is a meeting of people interested in libre technologies, focused on sharing knowledge and experience around them.

After two virtual editions due to the current situation, it finally looks like we will be able to hold the event in person again. As a result of the call for open venue proposals, it will finally be carried out from the one submitted by **[GALPon - Grupo de Amigos de Linux de Pontevedra](https://gitlab.com/eslibre/coord/-/blob/master/propuestas/2022/galpon-grupo-de-amigos-de-linux-de-pontevedra.md)**. **[GALPon](https://www.galpon.org/)** is a non-profit cultural association that for more than 18 years has been dedicated to promoting and disseminating the use of the **libre software in Galician society**. Its main field of activity is the province of **Pontevedra, in Galicia**, Spain.; where they have led events of great relevance for libre software such as **[Akademy-es 2019](https://www.kde-espana.org/akademy-es-2019)**. In addition, also as co-organizers of this edition, they will have the support of **[AGASOL (Asociación de Empresas Galegas de Software Libre Agasol)](https://www.agasol.gal/)**, an entity with which they have been working historically also with the intention of improving the implementation of free culture in the different levels of the region.

### About the congress

This year the congress will have the option of being held **in person in Vigo** thanks to **GALPon** and **AGASOL**, as host communities, have taken charge of securing the necessary spaces in **[MARCO (Museum of Contemporary Art of Vigo)](https://www.marcovigo.com/)** and **[MUTA (Mutant Space for Events in the center of Vigo)](https://www.muta.gal/)**.

<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe><p><a href="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331">Ver pantalla completa</a></p>

The same format as in previous years will be followed: talks, workshops, community devrooms... with the support of associations such as **[Interferencias](https://interferencias.tech/)** y **[LibreLabGRX](https://librelabgrx.cc/)** and other agencies such as **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)** o **[Wikimedia España](https://www.wikimedia.es/)**, who will help the organization and join efforts in the realization of this edition in a hybrid face-to-face/virtual format. Finally, they will also count as sponsors with **[Xunta de Galicia](https://www.xunta.gal/)**, **[Librebit](https://www.librebit.com/es/)** and **[Asociación MeLiSA (Asociación de usuarios de software libre da Terra de Melide)](https://www.melisa.gal/)**.

The **submission of proposals is now officially closed**, So you can inform yourself about all the activities of the congress on the following pages:

- **Complete schedule**: <https://eslib.re/2022/horario/>
- **Accepted proposals**: <https://propuestas.eslib.re/2022/>

### How to get there

If **you are coming from outside Vigo by plane** we recommend you not to take a taxi directly from the airport without stopping to check the public transport:
- To begin with, the **city bus** is much cheaper: the ticket costs €1.35 compared to the "minimum" of around €22 for a taxi ride. Recommending to have the exact amount or to take into account that you cannot pay with notes of more than €10.
- [The **line A** buses](https://www.vitrasa.es/renovaciones-85_A) pass through the airport **every 30-35 minutes**, simply take a bus from that line, and if you get off at the stop in **Rúa de Urzaiz & Rúa do Príncipe** (a journey of about 30 minutes), you will already be right outside MARCO. We leave you [here](/2022/assets/pdf/Vitrasa.pdf) also a PDF with the stops of line A that you may be interested in highlighted.
- You can also plan your outbound and return journeys using the Moovit website.
  * On arrival: [Aeropuerto de Vigo (VGO) → Rúa Do Príncipe, Vigo](https://moovitapp.com/vigo-3841/poi/R%C3%BAa%20Do%20Pr%C3%ADncipe/Aeropuerto%20de%20Vigo%20(VGO)/es?tll=42.236517_-8.721962&fll=42.224978_-8.632835&utm_medium=link&utm_source=shared&customerId=6863)
  * For the return trip: [Rúa Do Príncipe, Vigo → Aeropuerto de Vigo (VGO)](https://moovitapp.com/vigo-3841/poi/Aeropuerto%20de%20Vigo%20(VGO)/R%C3%BAa%20Do%20Pr%C3%ADncipe/es?tll=42.224555_-8.631747&fll=42.236517_-8.721962&utm_medium=link&utm_source=shared&customerId=6863)
- In any case, you can also consult all the lines on the website of the company responsible for urban public transport in the city of Vigo: [VITRASA](https://www.vitrasa.es/renovaciones) or use [this page](https://www.vitrasa.es/php/index.php?pag=calculo-rutas) to calculate routes between different stops.

If **you also come from outside Vigo but by train**, once you leave the station you will be practically in the center of the city, and in just 10 minutes walking you will be in the MARCO.

### Tourism

If you want to take the opportunity to do some sightseeing, you can find interesting information on the website of the [Concello de Vigo](https://www.turismodevigo.org/es). Concretely, the "Architecture Walks" are very suggestive, of which the Concello itself highlights the following (also all near MARCO):

- [Vigo's Old Town: explore narrow streets and discover historical buildings](https://www.turismodevigo.org/en/vigos-old-town)
- [Seafaring Vigo: a route between sea and stone](https://www.turismodevigo.org/en/seafaring-vigo)
- [Stately Vigo: take a walk through the bourgeois part of the city](https://www.turismodevigo.org/en/stately-vigo)
- [Vigo's past and present: the city along seven centuries](https://www.turismodevigo.org/en/vigos-past-and-present)

### Exhibitions

In the MARCO itself:
- In the exhibition hall on the first floor, an exhibition of paintings, drawings and sculptures by Alfredo Alcain
- In the exhibition hall on the ground floor, an exhibition of "Lo antropomórfico 1996-2022" by Francisco Leiro.

+info at:
- <https://www.marcovigo.com/es/actuais>
- <https://www.nosdiario.gal/articulo/cultura/corpos-atormentados-francisco-leiro/20220525091846144186.html>

### Attendee Registration

<p style="padding-top: 5px; text-align: center; font-size: 1.25rem;"><strong>Attendance at the event is <span class="red">open and free</span>, but due to capacity issues we need you to register <a href="https://eventos.librelabgrx.cc/events/2e86b318-eed2-4e95-84da-42f8ea0337f9" target="_blank">here</a> if you plan to attend.</strong></p>

<p style="text-align: center;">If you have any questions, do not hesitate to write to us at <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</p>

<h3 style="padding-top: 10px; text-align: center;">SEE YOU SOON!!!!!<br>👏👏👏</h3>
