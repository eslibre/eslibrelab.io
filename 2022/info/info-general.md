---
layout: 2022/default
section: info
permalink: /2022/info-general
title: Acerca de esLibre 2022
---

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edición 2022
</h1>

<p align="center">
  <cite style="color: red;">Página con información que se irá actualizando regularmente.</cite>
</p>

**es<span class="red">Libre</span>** es un encuentro de personas interesadas en las tecnologías libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas.

Después de dos ediciones virtuales a raíz de la actual situación, finalmente parece que podremos volver a realizar el evento de forma presencial. Como resultado de la llamada a propuestas de sede abierta, finalmente se llevará a cabo de la presentada por **[GALPon - Grupo de Amigos de Linux de Pontevedra](https://gitlab.com/eslibre/coord/-/blob/master/propuestas/2022/galpon-grupo-de-amigos-de-linux-de-pontevedra.md)**. **[GALPon](https://www.galpon.org/)** es una asociación cultural sin ánimo de lucro que lleva ya más de 18 años dedicándose a promover y difundir el uso del **software libre en la sociedad gallega**. Su principal campo de actuación es la provincia de **Pontevedra, en Galicia**, España; donde han llevado eventos de gran relevancia para el software libre como es **[Akademy-es 2019](https://www.kde-espana.org/akademy-es-2019)**. Además, también como coorganizadores de esta edición, contarán con el apoyo de **[AGASOL (Asociación de Empresas Galegas de Software Libre Agasol)](https://www.agasol.gal/)**, una entidad con la que han venido trabajando históricamente también con la intención de mejorar la implantación de la cultura libre en los diferentes estamentos de la región.

### Sobre el congreso

Este año el congreso tendrá la opción de realizarse **presencialmente en Vigo** gracias a **GALPon** y **AGASOL**, como **comunidades anfitrionas**, se han hecho cargo de conseguir los espacios necesarios en **[MARCO (Museo de Arte Contemporáneo de Vigo)](https://www.marcovigo.com/)** y **[MUTA (Espacio Mutante para Eventos en el centro de Vigo)](https://www.muta.gal/)**.

<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe><p><a href="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331">Ver pantalla completa</a></p>

Se seguirá el mismo formato que en años anteriores: charlas, talleres, salas de comunidades... contando con el apoyo de asociaciones como **[Interferencias](https://interferencias.tech/)** y **[LibreLabGRX](https://librelabgrx.cc/)** y otros organismos como la **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)** o **[Wikimedia España](https://www.wikimedia.es/)**, que ayudarán a la organización y sumarán esfuerzos en la realización de esta edición en formato híbrido presencial/virtual. Por último, también contarán como patrocinadores con la **[Xunta de Galicia](https://www.xunta.gal/)**, **[Librebit](https://www.librebit.com/es/)** y la **[Asociación MeLiSA (Asociación de usuarios de software libre da Terra de Melide)](https://www.melisa.gal/)**.

El **envío de propuestas ya está oficialmente cerrado**, así que puedes informarte sobre todas las actividades del congreso en las siguientes páginas:

- **Horario completo**: <https://eslib.re/2022/horario/>
- **Propuestas aceptadas**: <https://propuestas.eslib.re/2022/>

### Cómo llegar

Si **vienes de fuera de Vigo en avión** te recomendamos que no cojas directamente un taxi para llegar desde el aeropuerto sin pararte a mirar antes el transporte público:
- Para empezar, el **autobús urbano** es mucho más barato: el billete cuesta 1.35€ frente al "mínimo" de los alrededor de 22€ por el trayecto en taxi. Recomendando tener el importe exacto o tener en cuenta que no se puede pagar con billetes de importe superior a 10 €.
- [Los autobuses de la **línea A**](https://www.vitrasa.es/renovaciones-85_A) pasan por el aeropuerto **cada 30-35 minutos**, simplemente coge un bus de dicha línea, y si te bajas en la parada de **Rúa de Urzaiz & Rúa do Príncipe** (un trayecto de unos 30 minutos), ya estarás justo en la puerta de MARCO. Os dejamos [aquí](/2022/assets/pdf/Vitrasa.pdf) también un PDF con las paradas de la línea A que os pueden interesar resaltadas.
- También puedes planificar tus trayectos tanto para la ida como para la vuelta usando desde la página de Moovit.
  * A la llegada: [Aeropuerto de Vigo (VGO) → Rúa Do Príncipe, Vigo](https://moovitapp.com/vigo-3841/poi/R%C3%BAa%20Do%20Pr%C3%ADncipe/Aeropuerto%20de%20Vigo%20(VGO)/es?tll=42.236517_-8.721962&fll=42.224978_-8.632835&utm_medium=link&utm_source=shared&customerId=6863)
  * Para la vuelta: [Rúa Do Príncipe, Vigo → Aeropuerto de Vigo (VGO)](https://moovitapp.com/vigo-3841/poi/Aeropuerto%20de%20Vigo%20(VGO)/R%C3%BAa%20Do%20Pr%C3%ADncipe/es?tll=42.224555_-8.631747&fll=42.236517_-8.721962&utm_medium=link&utm_source=shared&customerId=6863)
- De todas formas, también puedes consultar todas las líneas en la página de la empresa responsable del transporte público urbano en la ciudad de Vigo: [VITRASA](https://www.vitrasa.es/renovaciones) o usar [esta página](https://www.vitrasa.es/php/index.php?pag=calculo-rutas) para calcular rutas entre distintas paradas.

Si **venís también de fuera de Vigo pero en tren**, una vez salgáis de la estación estaréis prácticamente en el centro de la ciudad, y en apenas 10 minutos caminando estará en el MARCO.

### Turismo

Si queréis aprovechar para hacer también algo de turismo, podéis encontrar información interesante en la página del [Concello de Vigo](https://www.turismodevigo.org/es). Concretando son muy sugerentes los "Paseos por la Arquitectura", de los que el propio Concello destaca los siguientes (además todo cerca de MARCO):

- [Vigo Antiguo: adéntrate por callejuelas rodeadas de edificios históricos](https://www.turismodevigo.org/es/vigo-antiguo)
- [Vigo Marítimo: un recorrido entre el mar y la piedra](https://www.turismodevigo.org/es/vigo-maritimo)
- [Vigo Señorial: callejea por la ciudad burguesa](https://www.turismodevigo.org/es/vigo-senorial)
- [Vigo de ayer a hoy: La ciudad a través de 7 siglos](https://www.turismodevigo.org/es/vigo-de-ayer-hoy)

### Exposiciones

En el propio MARCO:
- En la sala de exposiciones de la primera planta, exposición de pinturas dibujos y esculturas de Alfredo Alcain
- En la sala de exposiciones de la planta baja exposición de «Lo antropomórfico 1996-2022» de Francisco Leiro.

+info en:
- <https://www.marcovigo.com/es/actuais>
- <https://www.nosdiario.gal/articulo/cultura/corpos-atormentados-francisco-leiro/20220525091846144186.html>

### Registro asistentes

<p style="padding-top: 5px; text-align: center; font-size: 1.25rem;"><strong>La asistencia al evento es <span class="red">libre y gratuita</span>, pero por cuestiones de aforo necesitamos que te registres <a href="https://eventos.librelabgrx.cc/events/2e86b318-eed2-4e95-84da-42f8ea0337f9" target="_blank">aquí</a> si tienes pensado asistir.</strong></p>

<p style="text-align: center;">Si tienes cualquier duda, no dudes en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</p>

<h3 style="padding-top: 10px; text-align: center;">¡¡¡NOS VEMOS DENTRO DE POCO!!!<br>👏👏👏</h3>
