---
layout: 2021/post
section: post
category: 2021
title: Publicado programa de esLibre 2021
---

<h2 style="text-align:center">¡Ya está publicado el horario de la edición de este año de <strong>es<span class="red">Libre</span> 2021</strong>!</h2>

<br>Son casi 100 propuestas las que se han juntado en el programa de este año, entre las 2 pistas propias del congreso y las 9 salas presentadas por diferentes comunidades (además del tablón de proyectos y la sección de artículos). Nos alegramos de que un congreso de la gente para la gente, orientado a crear una comunidad donde todas las personas interesadas en tecnologías libres y cultura libres tengan cabida, con el único requisito de ser respetuosos con los demás, sea cada vez más grande.

<br>Es dificil que no encuentres algo que te llame la atención, así que simplemente visita <a href="/2021/programa/" target="_blank">la página del programa</a> y <strong>los días 25 y 26 de junio solo preocúpate de disfrutar del conocimiento libre</strong>.
