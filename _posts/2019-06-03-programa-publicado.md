---
layout: 2019/post
section: post
category: 2019
title: Programa esLibre 2019 publicado
---

Ya está el programa de <strong>es<span class="red">Libre</span></strong> 2019 disponible en la web!
* [https://eslib.re/2019/programa/](https://eslib.re/2019/programa/)

Finalmente la programación se compone de charlas en 7 salas temáticas:

- Track 1: Dev + Web
- Track 2: Social
- Track 3: Big Data, Machine Learning and Cloud
- Devroom 1: <a href="{{ '/2019/programa/devrooms/sl_uni.html' | prepend: site.baseurl }}" target="_blank">Software Libre en la universidad</a>
- Devroom 2: <a href="{{ '/2019/programa/devrooms/privacidad.html' | prepend: site.baseurl }}" target="_blank">Privacidad, descentralización y soberanía digital</a>
- Devroom 3: <a href="{{ '/2019/programa/devrooms/perl.html' | prepend: site.baseurl }}" target="_blank">Perl</a>
- Devroom 4: <a href="{{ '/2019/programa/devrooms/inf_mat.html' | prepend: site.baseurl }}" target="_blank">Informática y matemáticas</a>
