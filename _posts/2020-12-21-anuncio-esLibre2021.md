---
layout: 2021/post
section: post
category: 2021
title: esLibre 2021
---


Antes de nada, desde LibreLabUCM nos gustaría felicitar a la organización de esLibre 2020
por la edición anterior que fue todo un éxito apesar de las dificultades que vivimos.

Somos LibreLabUCM, una asociación formada por entusiastas del software libre y la ciberseguridad.
Desde hace tiempo que seguimos esLibre y por ello nos hemos candidatado este año a organizarlo, ya que
también difundimos los principios y objetivos del evento. ¿Por qué no candidatarse?

La edición esLibre 2021, será online al igual que la edición anterior aunque contará con algunas
novedades/diferencias que iremos anunciando a lo largo de los próximos meses.

Para finalizar esta entrada, agradecer también a LibreLabGRX, Interferencias, la OfiLibre del Conocimiento y
Cultura Libre y HackMadrid%27 por colaborar con el evento.

