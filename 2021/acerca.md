---
layout: 2021/default
section: acerca
title: Acerca de esLibre
---

<p style="padding-top: 1em" align="center">
  <cite style="color: black;">
    <strong><span class="red">ATENCIÓN</span></strong>: Envío de propuestas abierto hasta el 4 de junio. Toda la información <a href="/2021/propuestas/">aquí</a>.
  </cite>
</p>

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edición 2021
</h1>

**es<span class="red">Libre</span>** es un encuentro de personas interesadas en las tecnologías libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas.

Después de una [primera edición físicamente en Granada](/2019) (gracias a [LibreLabGRX](https://librelabgrx.cc)) y una segunda virtualmente desde [Madrid](/2020) (gracias a la [OfiLibre de la URJC](https://ofilibre.gitlab.io/)), volvemos otro año con la edición del año 2021. La próxima edición se celebrará virtualmente también como resultado de la propuesta presentada por **[LibreLabUCM](https://librelabucm.org/)**. Para que podáis tener algo más de información provisionalmente os dejamos aquí el contenido de dicha propuesta.

## Propuesta para la Celebración Virtual de **es<span class="red">Libre</span> 2021**

### Equipo organizador

El equipo organizador de esta propuesta estará compuesto en el equipo de organización local por:

-   Rafael Mateus Marques
-   Cristóbal Saraiba Torres
-   Pedro Javier Fernández

### Experiencia y motivación

La asociación LibreLabUCM se dedica a la difusión del software y conocimiento libre mediante la realización de charlas/talleres/eventos en diferentes lugares como facultades, institutos y también en diversos congresos.

Nuestra experiencia es amplia, abarcando la organización de charlas y talleres, tanto en formato presencial como online, entre ellas destacar eventos como:

-   [BufferHack 2019](https://bufferhack.librelabucm.org/)
-   Semana del Software Libre
-   [HacktoberDay 2020](https://hacktoberday.librelabucm.org/)
-   Colaboración, entre otros eventos, en esLibre 2019

Entre nuestras contribuciones al software libre se encuentran instancias de multiples servicios libres tales como plataformas de streaming como:

-   PeerTube
-   Redes federadas (Mastodon)
-   Aplicaciones para multiconferencias (Mumble)

Ademas de estos servicios tambien hosteamos varios mirrors oficiales de SO entre ellos:

-   Trisquel
-   F-Droid
-   Debian
-   ArchLinux
-   CentOS

Puedes acceder a una lista actualizada con todos nuestros mirrors y servicios [aquí](https://librelabucm.org/index.php/nuestros-proyectos/).

### Formato del congreso

Nuestra propuesta está basada en el formato de las ediciones anteriores junto a algunas modificaciones. El evento se realizaría de manera virtual y proponemos las siguientes pistas (que a excepción de la principal, se producirán de forma simultánea):

-   **Pista Principal:**
    -   En este pista se realizarán tanto el acto de bienvenida como el de clausura, así como las sesiones plenarias.
-   **Pistas Charlas:**
    -   Habrá tantas pistas como sea necesario para dar cabida a todas las [charlas aceptadas](https://propuestas.eslib.re/2021/charlas/).
-   **Pistas Talleres:**
    -   De haber suficientes [talleres aceptados](https://propuestas.eslib.re/2021/talleres/) de diferentes tipos, se agruparan en distintas pistas en función de la duración de los mismos: duración corta (hasta 45 minutos aprox) y mayor duración (hasta 2 horas aprox).
-   **Pistas Salas:**
    -   Existirán [salas organizados por diferentes comunidades](https://propuestas.eslib.re/2021/) y que organizarán sus propios programas.
-   **Sección de artículos y tablón de proyectos:**
    -   Se habilitará una zona en la web para [exponer diferentes artículos](https://propuestas.eslib.re/2021/articulos/) o darán facilidades para fomentar el trabajo y la discusión en torno a [proyecto presentados](https://propuestas.eslib.re/2021/proyectos/).

### Posibles fechas

Proponemos que el congreso se realice totalmente a finales de junio. Nos comprometemos en la medida de lo posible a evitar que la fecha final del congreso choque con la de otro congreso de temática similar.

### Aspectos económicos

La asistencia al evento será gratuita. Aunque podríamos buscar posible financiación de empresas privadas (nos comprometemos a que no sean por parte de compañías cuya actividad pudiera ser contraria a la filosofía del evento) y presupuestos/subvenciones de instituciones públicas. En caso de conseguir patrocinio, este se invertiría en algún sorteo de diferentes dispositivos relacionados con el software libre o algún objeto con el logotipo de esLibre como agradecimiento para ponentes (tazas, camisetas o similares).

### Facilidades disponibles

Para la realización del evento se dispone actualmente de:

-   Instancias propias de [BigBlueButton](https://bigbluebutton.org/).
-   Instancias propias de de [Jitsi](https://meet.jit.si/).
-   Chat para la realización de preguntas/dudas.
-   Personas voluntarias para hacerse cargo de las labores de moderación y gestión de la retransmisión en cada pista.

### Aspectos logísticos

Relación con otras comunidades tecnológicas y/o divulgativas que pueden colaborarán con la organización:

-   Interferencias
-   LibreLabGRX
-   HackMadrid%27
-   BitUp Alicante
-   Core UPM
-   Gul UC3M

Nos comprometemos a hacer un informe de valoración post-evento, que además de servir para conocer la evolución del mismo, pueda ayudar a futuros equipos organizadores.

Disponemos de varios servidores que actualmente soportan sin problema varios de los servicios que ofrecemos y que en caso de celebrar el evento desviaríamos temporalmente parte de esa capacidad a hostear las distintas salas del evento.

-   Las charlas, salas y actos plenarios pueden ser grabados
-   El software es accesible
-   Planes para actividades comunitarias fuera del horario del congreso (reuniones virtuales):\*
    -   Existirán salas virtuales (Mozilla Hubs)
    -   Además, tenemos pensado organizar algunos torneos de juegos libres para fomentar así la participación en el evento.

### Presentación de propuestas

Para la presentación de propuestas hemos decidido mantener [el mismo formato del año pasado](/2021/propuestas/).
