---
layout: 2021/default
section: info
title: Información práctica sobre esLibre
---

<h1 class="mt-3 mb-3">Información práctica</h1>

**es<span class="red">Libre</span> 2021** se celebrará de forma virtual los días viernes 25 y sábado 26 de junio de 2021\. Próximamente incluiremos en esta página información de interés para las personas que quieran asistir al congreso, pero mientras, recuerda que **el [envía de propuestas](/2021/propuestas) ya está abierto** para la edición virtual de este año.

Si tienes cualquier duda, no dudes en escribirnos por correo a [hola@eslib.re](mailto:hola@eslib.re), pero también puedes unirte a nuestro grupo de conversación desde [Matrix](https://matrix.to/#/#esLibre:matrix.org) o [Telegram](https://t.me/esLibre), o escribirnos por redes sociales desde [Mastodon](https://floss.social/@eslibre/) o [Twitter](https://twitter.com/esLibre_).

<h3 style="padding-top: 10px; padding-bottom: 245px; text-align: center;">¡¡¡TE ESPERAMOS!!!</h3>
