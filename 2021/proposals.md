---
layout: 2021/default
section: propuestas
title: Proposal submission for esLibre 2021
description: We are interested in knowing what you do. Send us proposals!
---

<h1 class="mt-3 mb-3">Submission of proposals for <strong>es<span class="red">Libre</span></strong> 2021</h1>

<p align="center">
  <cite style="color: black;">Puedes encontrar una versión en español de este texto <a href="/2021/propuestas/">aquí</a>.</cite>
</p>

<p>
  <cite style="color: grey;">Do you know someone who might want to speak at the conference? Send this page to whoever you want, we will appreciate it a lot if you help us reach more people.</cite>
</p>

The [**es<span class="red">Libre</span>** congress](https://eslib.re) is a meeting of _people interested in [free technologies](https://www.gnu.org/philosophy/free-sw.en.html) and [free culture](https://en.wikipedia.org/wiki/Free-culture_movement), focused on sharing knowledge and experiences around them_. The next edition will be broadcast online on Friday, June 25 and Saturday, June 26; this time being the host community **[LibreLabUCM](https://librelabucm.org/)**, but also with the support of the communities that have already organized past editions: **[Interferencias](https://interferencias.tech/)**, **[LibreLabGRX](https://librelabgrx.cc)** and **[URJC OfiLibre](https://ofilibre.gitlab.io/)**.

At **es<span class="red">Libre</span>** we try to make an annual event where everyone is invited to participate, the only thing that you needs is to have something to say about the world of technologies and free culture. We are interested in people of all ages, experiences and profiles, whether technical or user. We are eager to hear and learn from everyone and our main goal is to reach communities of all kinds.

<h3 class="mt-3 mb-3" id="topic">Topic</h3>

**Free software** is the central theme of **es<span class="red">Libre</span>**, but what we really want is to achieve an event where we find both technical and other activities of a more informative level, or even that give rise to discuss different points of view on aspects of the world of the free knowledge. This may include comments on publishing licenses, applications and development of various technologies, interests and concerns that different communities may want to present, or the exploration and dissemination of free art.

In general terms, _topics directly related to works (software, hardware, culture, etc.) that are distributed with free licenses_, understanding them according to the [FSF definition of free software](https://www.gnu.org/philosophy/free-sw.en.html) or the [OSI definition of open source](https://opensource.org/osd); or in more informative scope, that it is understood that they meet the [definition of free cultural work](https://freedomdefined.org/Definition): all of this is what is considered to fit perfectly in esLibre. Do you have something in mind? Propose it!

Although there is a lot of free software (and in general free works) that is distributed commercially without this presenting any problem, we want to keep **es<span class="red">Libre</span>** as a neutral forum with respect to commercial interests. <u>In general, talks, workshops or any other type of activity that specifically promotes a commercial offer will not be accepted</u>.

<h3 class="mt-3 mb-3" id="types">Types of proposals</h3>

There are several types of contributions that you can propose (but not only):

-   **Talks**. "Traditional" presentation, in normal format (25 min. approx.) or lightning talk (10 min. approx.). The organization may propose changes in the format of any of the talks, for organizational or content reasons. You may also, exceptionally, propose a longer format to a talk.

-   **Workshops**. Practical presentation, "hands on". It can be in many formats, from demonstrations where those who attend can follow a kind of "guided tour" through the theme of the workshop, to introductory sessions to a technology where they can experiment with it; in general, any practical format that you might consider interesting to make something known.

-   **Devrooms**. You can also propose the program of a devroom, which would take place in parallel with the rest of the congress activities. Normally a devroom will be organized by and/or for a community, and will usually be specialized in a topic (although it can also be generalist). Just as an example, in previous editions we had rooms that dealt with more general topics such as free software at the University, privacy and digital rights or the dissemination of free culture; and other more specific ones such as Perl/Raku, GNOME, functional programming or technologies for the promotion of the Andalusian language (a region of southern Spain that has its own linguistic variety).

-   **Papers section**. You can consider this as a section of "virtual posters", where we will dedicate a section of the web to expose all those publications that you have made and that you would like people to know, giving if you want a means of contact through which they could ask you questions or simple comments. In addition, it would be used to announce them in the congress breaks.

-   **Projects board**. If there is a free software project in any field that you would like to give relevance to, but you prefer it to be in a more informal way or without having to assume the focus of attention as in the previous types of activity, you can also propose it and we will put the means to bring together people who may be interested in this same project.

-   **Other activities**. Use your imagination! Make us proposals with other formats, we are very interested in exploring other ways of sharing knowledge (and even more so now that we are not separated by physical barriers 🐧).

<h3 class="mt-3 mb-3" id="how">I am interested in giving a talk but ... how?</h3>

Have you never given a talk but want to debut? In this event we would love to accompany you on that adventure. We are aware that giving a talk can be an important challenge and that is why we want to give you a series of guidelines that will help you to exhibit here (or wherever). We leave you a list of resources to expose without using a "powerpoint" (🙃): [Markdown](https://www.markdownguide.org/basic-syntax/), [LaTeX](https://en.wikibooks.org/wiki/LaTeX), [Marp](https://gist.github.com/yhatt/a7d33a306a87ff634df7bb96aab058b5), [Pandoc](https://pandoc.org/getting-started.html)...

-   The folks at TED have created a [video list](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) that teach you how to make a presentation.

-   Being an online conference, you will need at least a microphone, a webcam and a computer (or at least a mobile phone). You can practice speaking in front of the computer by calling your family or friends on [Jitsi](https://meet.jit.si/) and it will be almost the same as when you do it for real.

-   Don't write a lot of text in the presentation, make a list of key points and develop them by talking. As it is online, you can have a cheat sheet and no one will notice (😉), but try not to read all the time or it will not be as fluid. Let yourself go! Think that you are in a conversation with someone close.

-   So that the attendees who listen to you can follow you better, create an index exposing the ideas you want to talk about and follow that order during the talk.

-   End with a summary that give the people who are listening to you an idea to take into their lives, this way you will create a much more interesting impact.

Again, these are all suggestions. Create your presentation as you want and if you need more guidance and help, do not hesitate to ask [the organization](mailto:propuestas@eslib.re) for it. To inspire you, we leave you a list of online talks that you can watch right now:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

There are a lot of cool free projects out there to participate in, or use. You can start [looking for a free operating system](https://distrowatch.com/) that fits your personality, read some web development projects in a [libre blog](https://dev.to/) or libre-metasoftware in [this other](http://www.elbinario.net/). You can also explore the different free social networks of the [Fediverse](https://fediverse.party/) like [Mastodon](https://joinmastodon.org/). Take a tour of these projects and if you like them, come to the event to learn about many more.

<h3 class="mt-3 mb-3" id="diversity">Our commitment to diversity</h3>

**The organization [is committed](/conduct) to creating a safe, non-toxic and diverse environment**, so we'd love to give a voice to speakers from the various minority communities who use or develop free software, hardware, or culture. We are aware that there are many people who use and create free technologies and actively support the dissemination of free culture, and many times they are not heard, we want to change that: we offer our microphone and space for people to speak from these communities.

We want to learn and create bridges, so that technology is (finally) a support and collaboration tool. If you think that in your community you have something to tell from the development or experience using free software, let us know. We are looking for a collaborative, multidisciplinary and libre future, help us to achieve it!

<h3 class="mt-3 mb-3" id="dates">Dates</h3>

-   Proposals deadline: **June 4**
-   Devrooms program deadline: **June 11** (by that day programs should be ready)
-   Publication of the final program: **June 11**
-   Celebration of the **es<span class="red">Libre</span>** Congress: **June 25 and 26**

**All deadlines close at 23:59 Madrid time (CET).**

<h3 class="mt-3 mb-3" id="submit">How to submit your proposal</h3>

The submission of proposals is open to everyone, and although we will continue to use **GitLab** as a platform to receive proposals for a matter of **transparency and feedback**, you can also send proposals through the forms that you can find in the following links:

-   ̶T̶a̶l̶k̶s̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶W̶o̶r̶k̶s̶h̶o̶p̶s̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶D̶e̶v̶r̶o̶o̶m̶s̶ ̶p̶r̶o̶p̶o̶s̶a̶l̶s̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶P̶a̶p̶e̶r̶s̶ ̶s̶e̶c̶t̶i̶o̶n̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶P̶r̶o̶j̶e̶c̶t̶ ̶b̶o̶a̶r̶d̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶O̶t̶h̶e̶r̶ ̶a̶c̶t̶i̶v̶i̶t̶i̶e̶s̶  <span class="red">(link deactivated due to deadline)</span>

In addition, several of the devrooms already accepted also have their own proposal submission open:

-   ̶A̶λ̶h̶a̶m̶b̶r̶a̶ ̶D̶a̶y̶:̶ ̶P̶r̶o̶g̶r̶a̶m̶a̶c̶i̶ó̶n̶ ̶f̶u̶n̶c̶i̶o̶n̶a̶l̶ ̶p̶a̶r̶a̶ ̶t̶o̶d̶o̶ ̶e̶l̶ ̶m̶u̶n̶d̶o̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶D̶e̶r̶e̶c̶h̶o̶s̶ ̶D̶i̶g̶i̶t̶a̶l̶e̶s̶ ̶y̶ ̶P̶r̶i̶v̶a̶c̶i̶d̶a̶d̶ ̶e̶n̶ ̶I̶n̶t̶e̶r̶n̶e̶t̶ ̶+̶ ̶S̶o̶b̶e̶r̶a̶n̶í̶a̶ ̶D̶i̶g̶i̶t̶a̶l̶ ̶e̶n̶ ̶l̶a̶s̶ ̶A̶u̶l̶a̶s̶ ̶(̶F̶u̶e̶r̶a̶ ̶G̶o̶o̶g̶l̶e̶)̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶D̶e̶s̶a̶r̶r̶o̶l̶l̶o̶ ̶d̶e̶ ̶v̶i̶d̶e̶o̶j̶u̶e̶g̶o̶s̶ ̶c̶o̶n̶ ̶G̶o̶d̶o̶t̶  <span class="red">(link deactivated due to deadline)</span>
-   ̶E̶W̶O̶K̶:̶ ̶E̶d̶u̶c̶a̶t̶i̶o̶n̶ ̶W̶i̶t̶h̶ ̶O̶p̶e̶n̶ ̶K̶n̶o̶w̶l̶e̶d̶g̶e̶  <span class="red">(link deactivated due to deadline)</span>

All the proposals for <strong>es<span class="red">Libre</span></strong> that are sent through these forms are registered in our **[proposals repository](https://gitlab.com/eslibre/propuestas/-/merge_requests)**, and once they receive 3 positive votes from the organization, it will be officially accepted.If it were necessary to clarify any question or suggestion, it would also be done in that same place, so do not lose sight of it. In any case, <u>once you register your proposal using the form, you will receive an email with the information of the registered proposal and a direct link to your proposal within the repository</u>.

If you have problems of any kind or any kind of doubt, you can write to us at <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">COME UP TO PARTICIPATE!!!<br>👏👏👏</h3>
