---
layout: 2021/default
section: conducta
title: Código de Conducta en esLibre
---

<h1 class="mt-3">Código de Conducta</h1>

## Propósito

**es<span class="red">Libre</span>** cree en una **comunidad abierta** a todo el mundo. Como tal, nos comprometemos a proporcionar un **ambiente amigable, seguro y acogedor para todes** independientemente de su género, sexo, orientación sexual, capacidades/necesidades específicas, etnia o lugar de nacimiento, religión (¡incluso sistema operativo preferido o lenguaje de programación y editor de texto!)  

Buscamos **personas** que quieran hablar de cultura libre con empatía.

Este código de conducta pretende mostrar nuestra disposición a crear un entorno seguro y distendido en el evento, comprometiendo a participadoras/os, voluntarias/os y ponentes a ser responsables y respetuosas.

## Ciudadanía Open Source

Un objetivo adicional de este código de conducta es **incrementar la ciudadanía de código abierto** alentando a los participantes a reconocer y fortalecer las relaciones entre lo que hacemos y **la comunidad** en general.

En el servicio de este objetivo, los organizadores de **es<span class="red">Libre</span>** recogerán candidaturas de personas ejemplares a lo largo de todo el evento y reconocerán selectos participantes después de la conferencia en el sitio web.

Si ves a alguien que está haciendo un esfuerzo extra para asegurar que nuestra comunidad resulte acogedora, amable, y que anime a todos los participantes a contribuir en la mayor medida posible, lo queremos saber.

Puedes nominar a alguien comentándole a cualquier miembro de la organización durante todo el congreso.

## Gracias por...

-   Ser considerado, respetuoso y colaborativo.
-   Abstenerse de comportamiento degradante, discriminatorio o de acoso.
-   Ser consciente del entorno y de tus compañeras/os participantes. Alerta a organización si observas una situación peligrosa o crees que alguien necesita ayuda.
-   Participar de una manera auténtica y activa. Al hacerlo, estás ayudando a que **es<span class="red">Libre</span>** sea una realidad.
-   Ayudarnos a compartir el contenido y llegar a todo tipo de personas y entornos, ¡ese es el espíritu de **es<span class="red">Libre</span>**!
-   No mandar spam, contenido con copyright o en general contenido ilegal por los canales oficiales.

## Por favor, avisa si eres testigo de los siguientes comportamientos:

-   Acciones o palabras intimidatorias, acosadoras, abusivas, discriminatorias, despectivas o degradantes por cualquier asistente o participante de **es<span class="red">Libre</span>** y eventos relacionados.
-   Spam en charlas y grupos del evento, archivos sospechosos, pishing y malware.
-   Comentarios despectivos y amenazas relacionadas con el género.
-   Comentarios transfobos, TERFs y similar.
-   Comentarios despectivos y amenazas contra personas de cualquier orientación sexual.
-   Comentarios y amenazas racistas.
-   Comentarios y amenazas contra personas por su religión, creencia o espiritualidad, de cualquier tipo.
-   Comentarios y amenazas de naturaleza nacional socialista o de naturaleza política.
-   Uso no consentido y descontextualizado de desnudez o abuso de intimidad.
-   Acoso de cualquier tipo, contra alguien o un grupo de personas.
-   El uso de las imágenes no consentidas de personas.

El evento se guarda el derecho a ampliar esta lista con tal de proteger a las personas que participen y asistan al congreso.

## La organización de **es<span class="red">Libre</span>** se toma muy en serio este tema, y tomará las siguientes medidas de ser necesario:

-   Cualquier comportamiento alertado será rápidamente gestionado y no habrá excepciones ni para ponentes, organización, asistentes o participantes de **es<span class="red">Libre</span>**.
-   La organización se responsabiliza de crear un ambiente adecuado y seguro, y por tanto escucha sugerencias y no sólo avisos.
-   Si se considera adecuado, el evento se reserva el derecho a la _expulsión permanente_ de participantes en conductas tóxicas mencionadas anteriormente.

## ¿Qué hacer si eres testigo u objeto de un comportamiento inaceptable?

Si estás involucrado en un comportamiento inaceptable, eres testigo de otra persona con dicho comportamiento o tienes otras preocupaciones, por favor notifícalo a organización del evento tan pronto como sea posible.

En las ediciones virtuales, habrá personas indentificadas como parte de la administración en cada sala, puedes comentarle cualquier problema directamente. También podrás escribirnos a [conducta@eslib.re](mailto:conducta@eslib.re) o comunicarnos cualquier problema mediante el chat que se habilitará un chat durante el congreso con este mismo objetivo. Toda denuncia que sea recibida será eliminada una vez termine el congreso para garantizar la seguridad y el anonimato de las mismas.

**Recuerda**: El equipo de **es<span class="red">Libre</span>** estará disponible para ayudar a toda persona que participe en el congreso como contactos de seguridad local o para ofrecer cualquier tipo de ayuda a aquellos que experimentan un comportamiento inaceptable.

## ¡Gracias por ayudarnos a crear un ambiente seguro!

Esperamos seguros de que el evento irá genial, pero si ocurre algo estaremos ahí para gestionarlo rápidamente. Queremos crear una comunidad abierta y sana, así que necesitamos vuestra ayuda para eso: Disfruta del evento, comparte y aprende, desde organización nos ocuparemos de que todo el mundo se sienta a gusto.

## Información de contacto

Si te ves en la necesidad de contactar con nosotros, recuerda mediante los que puedes contactarnos en cualquier momento:

*   Correo electrónico: [hola@eslib.re](mailto:hola@eslib.re)
*   Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:matrix.org)
*   Telegram: [@esLibre](https://t.me/esLibre)
*   Mastodon: [@eslibre@floss.social](https://floss.social/@eslibre/)
*   Twitter: [@esLibre_](https://twitter.com/esLibre_)

## Licencia y Atribución

El código de conducta del evento se puede encontrar en [{{ site.url }}/conducta/](/conducta/) y es una adaptación según nuestro criterio y necesidades del impresionante trabajo del Open Source Bridge. El original está disponible en [http://opensourcebridge.org/about/code-of-conduct/](http://opensourcebridge.org/about/code-of-conduct/) y está liberado bajo una licencia [Creative Commons Atribución-CompartirIgual 4.0 Internacional (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.es).
