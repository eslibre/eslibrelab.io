---
layout: 2023/default
section: propuestas
permalink: /2023/propuestas
title: Envío de propuestas para esLibre 2023
description: Nos interesa conocer qué cosas haces. ¡Mándanos propuestas!
---

<h1 class="mt-3 mb-3">Envío de propuestas para <strong>es<span class="red">Libre</span></strong> 2023</h1>

<p align="center">
  <cite style="color: black;">You can find an English version of this text <a href="/2023/proposals">here</a>.</cite>
</p>

<p>
  <cite style="color: grey;">¿Conoces a alguien que podría querer hablar en el congreso? Envíale esta página a quien quieras, te lo agradeceremos un montón si nos ayudas a llegar a más gente.</cite>
</p>

<div style="width:100%">
    <div style="display: table; margin: 0 auto; width: 38%; background: ghostwhite; font-size: 20px; padding: 10px; margin-bottom: 15px; border: 1px solid lightgray;">
      • <a href="#tematica">Temática</a>
      <br>
      • <a href="#tipos">Tipos de propuestas</a>
      <br>
      • <a href="#como">Me interesa impartir una charla pero... ¿cómo?</a>
      <br>
      • <a href="#diversidad">Nuestro compromiso por la diversidad</a>
      <br>
      • <a href="#fechas">Fechas</a>
      <br>
      • <a href="#enviar">Cómo enviar tu propuesta</a>
    </div>
</div>

**es<span class="red">Libre</span>** es un encuentro de personas interesadas en la divulgación de la cultura libre y las tecnologías libres, tanto en forma de software libre como en forma de hardware libre, así como de las comunidades que hacen esto posible. La próxima edición será también presencial (aunque habrá opción de seguirla online) los días **viernes 12 y sábado 13 de mayo**, siendo en esta ocasión anfitrionas las comunidades de **[Vitalinux](https://wiki.vitalinux.educa.aragon.es/)** y **[migasfree](http://www.migasfree.org/)**.

En **es<span class="red">Libre</span>** intentamos mantener un evento anual donde todo el mundo está invitado a participar, lo único que necesita tener algo que decir sobre el mundo de las tecnologías y cultura libre. Nos interesan personas de todas las edades, experiencias y perfiles ya sean técnicas o usuarias. Estamos deseando escuchar y aprender de todas las personas y nuestro objetivo principal es llegar a comunidades de todo tipo.

<h3 class="mt-3 mb-3" id="tematica">Temática</h3>

**Software libre**, **hardware libre** y **cultura libre** son la temática central de **es<span class="red">Libre</span>**, pero lo que realmente queremos es conseguir un evento donde encontremos tanto actividades técnicas como otras de un nivel más divulgativo, o incluso que den pie a debates para compartir diferentes puntos de vista sobre aspectos del mundo del conocimiento libre. Esto puede comprender comentarios sobre licencias de publicación, aplicaciones y desarrollo de diversas tecnologias, intereses e inquietudes que quieran presentar diferentes comunidades, o la exploración y divulgación del arte y humanidades.

En líneas generales, _temas relacionados directamente con obras (software, hardware, cultura, etc.) que se distribuyan con [licencias libres](https://es.wikipedia.org/wiki/Licencia_de_software_libre)_; aunque no tiene que ser algo estríctramente tecnológico, toda propuesta que se incline más al ámbito más puramente divulgativo, pero que se entienda que cumplan la [definición de obra cultural libre](https://freedomdefined.org/Definition/Es) también encajaría perfectamente: **¿Tienes algo en mente? ¡Anímate!**

Aunque mucho software libre (y en general obras libres) se distribuye comercialmente sin que esto presente problema alguno, queremos mantener **es<span class="red">Libre</span>** como un foro neutro con respecto a los intereses comerciales y otras temáticas que consideremos "nocivas". <u>En general, no se aceptarán charlas, talleres ni ningún otro tipo de actividad que promocionen específicamente una oferta comercial o temáticas relacionadas con criptomonedas</u>.

<h3 class="mt-3 mb-3" id="tipos">Tipos de propuestas</h3>

Hay varios tipos de actividades que puedes proponer (pero no solo):

-   **Charlas**. Presentación típica que puede ser de unos 30 minutos aproximadamente de duración o en formato relámpago de unos 10-15 minutos (lo que puede ser ideal para presentar ese proyecto que tienes en mente o que todavía no has presentado nunca pero te gustaría escuchar opiniones 😉). La organización podrá proponer cambios de formato a alguna de las charlas, por motivos organizativos o de contenidos. También podrá, de forma excepcional, proponer a alguna charla un formato más largo.

-   **Talleres**. Exposición algo más práctica que puede ser en muchos formatos, desde demostraciones donde las personas que asistan puedan seguir una especie de "paseo guiado" por la temática del taller, hasta sesiones de iniciación a una tecnología donde se pueda experimentar con ella; en general, cualquier formato práctico que puedas considerar que sea interesante para dar a conocer algo.

-   **Salas (devroom)**. También puedes proponer el programa de una sala, que se realizaría en paralelo con el resto de actividades del congreso. Normalmente una sala estará organizada por una comunidad específica y estará centrada en un tema concreta. Por ejemplo, salas presentadas otros años:

    -   Software Libre en la Universidad
    -   Divulgación de la cultura libre
    -   Privacidad y derechos digitales
    -   Tecnologías para el fomento de la lengua andaluza
    -   Desarrollo en Perl/Raku, GNOME, programación funcional...
        <br><br>

-   **Mesas para comunidades**. Si perteneces a alguna comunidad involucrada con el **software libre**, el **hardware libre** o la **cultura libre** y te gustaría disponer de un espacio para hablar sobre tu comunidad de forma informal con la gente que pase por allí, habilitaremos una zona con mesas donde también dejar cosas como pegatinas, panfletos, chapas o similares para darla a conocer.

-   **Espacio misceláneo**. El conocimiento no solo se comparte en forma de charlas y talleres, así que os posible que también quieras participar de otra forma que no impliquen estos formatos, te dejamos aquí otros tipos de propuestas que podrías enviar para participar en **es<span class="red">Libre</span>** de una forma más libre:

    -   **Artículos**. Tendremos un apartado en la web donde exponer publicaciones realizadas que quieras compartir y que te gustaría que las personas pudieran conocer, pudiendo además establecer algún medio de contacto para plantear cuestiones o simples comentarios si así lo deseas.

    -   **Pósters**. Similar al anterior, pero apoyándose en un medio físico que se podría exponer durante los días del congreso. Lo que podría dar lugar a conversaciones sobre diferentes trabajos en caso de encontrar otras personas interesadas en la misma temática.

    -   **Tablón de proyectos**. Si hay un proyecto de software libre de cualquier ámbito sobre el que te gustaría que se hiciera eco, pero prefieres que sea de una forma más informal o sin tener que asumir el foco de atención como en los tipos de actividades anteriores, también puedes proponerlo y pondremos los medios para reunir gente que pueda estar interesada en el mismo proyecto.

    -   **Otros formatos**. ¡Usa tu imaginación! Propón otros formatos, tenemos mucho interés en explorar otras formas de compartir conocimiento 🐧.

<h3 class="mt-3 mb-3" id="como">Me interesa impartir una charla pero... ¿cómo?</h3>

¿Nunca has impartido una charla pero quieres estrenarte? En este evento nos encantaría acompañarte en esa aventura. Somos conscientes de que dar una charla puede ser un reto importante y por eso queremos darte una serie de pautas que te ayuden a exponer aquí (o donde sea). Te dejamos una lista de recursos para exponer sin usar un "powerpoint" (🙃): [Markdown](https://markdown.es/sintaxis-markdown/), [LaTeX](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Texto_completo), [Marp](https://www.genbeta.com/herramientas/marp-herramienta-que-nos-permite-crear-presentaciones-modo-texto-usando-markdown), [Pandoc](https://ondiz.github.io/cursoLatex/Contenido/15.Pandoc.html)...

-   La gente de TED ha creado una [lista de vídeos](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) que enseñan a hacer presentaciones.
-   No te preocupes, puedes tener una chuleta y nadie se dara cuenta (😉).
-   Para que las y los asistentes que te escuhen puedan seguirte mejor, crea un índice exponiendo las ideas de las que quieres hablar y sigue ese orden durante la charla.
-   Acaba con un resumen que deje en las personas que te están escuchando con una idea que llevarse a sus vidas, así podrás crear un impacto mucho más interesante.

De nuevo, todo esto son sugerencias. Crea tu presentación como quieras y si necesitas más guía y ayuda no dudes en preguntar por ella a [la organización](mailto:propuestas@eslib.re). Para inspirarte, te dejamos una lista de charlas online que puedes ver ahora mismo:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

Ahí fuera hay un montón de proyectos interesantes libres en los que participar, o usar. Puedes empezar [buscando un sistema operativo libre](https://distrowatch.com/) que vaya acorde con tu personalidad, leer algunos [proyectos de desarrollo web en un blog libre](https://dev.to/ben/devto-is-now-open-source-5n1) o de [metasoftware-libre en este otro](http://www.elbinario.net/). También puedes explorar las diferentes redes sociales libres del [Fediverso](https://fediverse.party/) como [Mastodon](https://joinmastodon.org/). Date un paseo por estos proyectos y si te gustan, ven al evento para aprender sobre muchos más.

<h3 class="mt-3 mb-3" id="diversidad">Nuestro compromiso por la diversidad</h3>

**La organización [se compromete](/conducta) a crear un ambiente seguro, no tóxico y diverso**, de modo que nos encantaría dar voz a ponentes de las varias comunidades minoritarias que hagan uso o desarrollen software, hardware o cultura libre. Somos conscientes de que hay muchas personas que usan y crean tecnologías libre y apoyan activamente la divulgación de la cultura libre, y muchas veces no son escuchadas, queremos cambiar eso: ofrecemos nuestro micrófono y espacio para que se hable desde estas comunidades.

Desde la organización queremos aprender y crear puentes, para que la tecnología sea (al fin) un herramientas de apoyo y colaboración: buscamos un futuro colaborativo, multidisciplinar y libre.

También buscamos que las diferentes comunidades nos hagan saber lo que quieran contar desde el desarrollo o la experiencia usando software libre, por eso también nos gustaría conocer iniciativas en esta línea de acción que podrían proponer desde el equipo de organización local. Algunas de estas líneas de las que trabajamos desde la organización general:

-   Establecer mecanismos activos a los que recurrir fácilmente para asegurarse de que el [código de conducta](/conducta) se cumple rápidamente ante cualquier comportamiento alertado que lo incumpla sin excepciones ni para ponentes, organización, asistentes o participantes de **es<span class="red">Libre</span>. Puedes encontrar todo esto detallado en la propia página del [código de conducta](/conducta)**.
-   La diversidad no debe centrarse únicamente en el género, por eso queremos fomentar que haya distintos tipos de participación que intenten disminuir el sentimiento de exclusión que puedan sentir las personas sin importar cualquier aspecto de su condición humana: origen étnico, religión, identidad de género, edad, habilidades físicas y experiencia u otras características de su identidad.
-   Queremos facilitar la participación de personas que tengan responsabilidades de cuidados, algo que aunque puede ser una barrera para la asistencia y envío de propuestas tanto para hombres como para mujeres, la responsabilidad a menudo recae en gran medida en las mujeres.
-   Realizar una comunicación activamente diversa e inclusiva mediante diferentes canales de comunicación, intentando que todo el material proporcionado por la organización se encuentre en formatos que sean fácilmente legibles por personas con algún tipo de dificultad de lectura.

Desde **[Etopia](https://etopia.es/)** nos han cedido varias **habitaciones en su residencia de invitados sin ningún coste** para participantes al congreso, por lo que queremos destinarlas a **mujeres, personas trans y otras personas de géneros infrarrepresentadas** en **es<span class="red">Libre</span>**, así como a **personas en situación de discapacidad, neurodivergentes y/o neurodiversas y otras situaciones similares**. Si perteneces a algunos de esos colectivos y quieres mandar una propuesta, puedes hacer la solicitud de alojamiento desde el propio formulario de [envío de propuestas](/2023/enviar/propuestas).

<h3 class="mt-3 mb-3" id="fechas">Fechas</h3>

-   1̶e̶r̶ ̶p̶e̶r̶i̶o̶d̶o̶ ̶d̶e̶ ̶e̶n̶v̶í̶o̶ ̶d̶e̶ ̶p̶r̶o̶p̶u̶e̶s̶t̶a̶s̶:̶ ̶3̶0̶ ̶d̶e̶ ̶s̶e̶p̶t̶i̶e̶m̶b̶r̶e̶ ̶a̶ ̶1̶5̶ ̶d̶e̶ ̶e̶n̶e̶r̶o̶   **<span class="red">[PLAZO CERRADO]</span>**
-   P̶l̶a̶z̶o̶ ̶d̶e̶ ̶e̶n̶v̶í̶o̶ ̶d̶e̶ ̶p̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶s̶a̶l̶a̶s̶:̶ ̶3̶0̶ ̶d̶e̶ ̶s̶e̶p̶t̶i̶e̶m̶b̶r̶e̶ ̶a̶ ̶1̶2̶ ̶d̶e̶ ̶f̶e̶b̶r̶e̶r̶o̶   **<span class="red">[PLAZO CERRADO]</span>**
-   2̶º̶ ̶p̶e̶r̶i̶o̶d̶o̶ ̶d̶e̶ ̶e̶n̶v̶í̶o̶ ̶d̶e̶ ̶p̶r̶o̶p̶u̶e̶s̶t̶a̶s̶:̶ ̶1̶6̶ ̶d̶e̶ ̶e̶n̶e̶r̶o̶ ̶a̶ ̶1̶9̶ ̶d̶e̶ ̶m̶a̶r̶z̶o̶   **<span class="red">[PLAZO CERRADO]</span>**
-   Envío del programa de salas: **hasta el 24 de marzo**

-   Publicación propuestas aceptadas 1<sup>er</sup> periodo: **22 de enero**  **<span class="blue">\[[ENLACE](https://propuestas.eslib.re/2023/)]</span>**
-   Publicación propuestas aceptadas 2º periodo (programa final): **26 de marzo**
-   Publicación horario definitivo: **2 de abril**
-   Celebración del congreso **es<span class="red">Libre</span>**: **12 y 13 de mayo**

<p style="padding-top: 0.5em; font-size: 1.0em" align="center">
  <strong>El límite de todas las fechas son a las 23:59 hora en Madrid (CET).</strong><br>
    <cite style="color: black;">
      <strong><span class="red">INFO</span></strong>: En <a href="https://propuestas.eslib.re/2023/" target="_blank">esta página</a> se publicarán las propuestas aceptadas al terminar los correspondientes periodos de envío de propuestas.
    </cite>
</p>

<h3 class="mt-3 mb-3" id="enviar">Cómo enviar tu propuesta</h3>

El envío de propuestas está abierta a todo el mundo, todas las propuestas recibidas podrán seguirse en todo momento por garantizar **transparencia y feedback**, y <u>se podrán enviar mediante los formularios que podrás encontrar en los siguientes enlaces</u>:

-   Programa general
    -    ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶c̶h̶a̶r̶l̶a̶s̶ ̶y̶ ̶t̶a̶l̶l̶e̶r̶e̶s̶   **<span class="red">[PLAZO CERRADO]</span>**
    -   P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶s̶a̶l̶a̶s̶   **<span class="red">[PLAZO CERRADO]</span>**
    -    ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶m̶e̶s̶a̶s̶ ̶p̶a̶r̶a̶ ̶c̶o̶m̶u̶n̶i̶d̶a̶d̶e̶s̶   **<span class="red">[PLAZO CERRADO]</span>**
    -    ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶m̶i̶s̶c̶e̶l̶á̶n̶e̶a̶s̶   **<span class="red">[PLAZO CERRADO]</span>**

-   Propuestas para salas
    -    ̶C̶o̶m̶u̶n̶i̶d̶a̶d̶ ̶D̶o̶c̶e̶n̶t̶e̶ ̶d̶e̶ ̶V̶i̶t̶a̶l̶i̶n̶u̶x̶:̶ ̶S̶o̶f̶t̶w̶a̶r̶e̶ ̶l̶i̶b̶r̶e̶ ̶y̶ ̶C̶o̶m̶p̶e̶t̶e̶n̶c̶i̶a̶ ̶D̶i̶g̶i̶t̶a̶l̶ ̶D̶o̶c̶e̶n̶t̶e̶   **<span class="red">[PLAZO CERRADO]</span>**
    -    ̶D̶i̶s̶t̶r̶o̶s̶ ̶L̶i̶n̶u̶x̶ ̶E̶s̶p̶a̶ñ̶a̶:̶ ̶E̶n̶c̶u̶e̶n̶t̶r̶o̶ ̶d̶e̶ ̶D̶i̶s̶t̶r̶i̶b̶u̶c̶i̶o̶n̶e̶s̶ ̶L̶i̶n̶u̶x̶   **<span class="red">[PLAZO CERRADO]</span>**
    -    ̶G̶e̶o̶i̶n̶q̶u̶i̶e̶t̶o̶s̶ ̶y̶ ̶c̶o̶m̶u̶n̶i̶d̶a̶d̶ ̶O̶p̶e̶n̶S̶t̶r̶e̶e̶t̶M̶a̶p̶ ̶-̶ ̶L̶i̶b̶r̶e̶G̶e̶o̶:̶ ̶G̶e̶o̶i̶n̶q̶u̶i̶e̶t̶o̶s̶,̶ ̶O̶p̶e̶n̶S̶t̶r̶e̶e̶t̶M̶a̶p̶,̶ ̶Q̶G̶I̶S̶ ̶y̶ ̶o̶t̶r̶a̶s̶ ̶a̶p̶l̶i̶c̶a̶c̶i̶o̶n̶e̶s̶ ̶l̶i̶b̶r̶e̶s̶ ̶e̶s̶p̶a̶c̶i̶a̶l̶e̶s̶   **<span class="red">[PLAZO CERRADO]</span>**

Todas las propuestas para <strong>es<span class="red">Libre</span></strong> que se envíen mediante esos formularios quedan registradas en nuestro **[repositorio de propuestas](https://gitlab.com/eslibre/propuestas/-/merge_requests?scope=all&state=opened&label_name[]=2023)**. Si fuera necesario aclarar cualquier cuestión o sugerencia también se haría en ese mismo lugar, por lo que no lo pierdas de vista. En cualquier caso, <u>una vez que registres tu propuesta mediante el formulario, te llegará un correo electrónico con la información de la participación registrada y enlace directo a tu propuesta dentro del repositorio</u>.

Si tienes problemas de cualquier tipo o cualquier tipo de duda, puedes escribirnos a <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">¡¡¡ANÍMATE A PARTICIPAR!!!<br>👏👏👏</h3>
