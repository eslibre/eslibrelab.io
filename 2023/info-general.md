---
layout: 2023/default
section: info
permalink: /2023/info-general/
title: Acerca de esLibre 2023
---

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edición 2023
</h1>

<p align="center">
  <cite style="color: red;">Página con información que se irá actualizando regularmente.</cite>
</p>

**es<span class="red">Libre</span>** es un encuentro de personas interesadas en la divulgación de la cultura libre y las tecnologías libres, tanto en forma de software libre como en forma de hardware libre, así como de las comunidades que hacen esto posible.

Después de una vuelta a la presencialidad por todo lo alto gracias a **[GALPon](https://www.galpon.org/)** y **[AGASOL](https://www.agasol.gal/)** hay que seguir haciendo crecer la comunidad, por lo que esta 5ª edición (y 3ª presencial) de **es<span class="red">Libre</span>** tendrá como comunidades anfritrionas a **[Vitalinux](https://wiki.vitalinux.educa.aragon.es/)** y **[migasfree](http://www.migasfree.org/)** para volver a celebrar estas jornadas de conocimiento libre.

* **[Vitalinux](https://wiki.vitalinux.educa.aragon.es/)** es un sistema operativo libre basado en versión ligera del sistema operativo GNU/Linux Ubuntu (Lubuntu), que además, es usado por los centro educativos de Aragón.
* **[migasfree](http://www.migasfree.org/)** es también un proyecto de software libre que nos permite gestionar el software de nuestros equipos de manera remota, desatendida y automatizada, estando del cliente de este ya integrado en **Vitalinux**.
* Podremos conocer más sobre estos proyectos durante el congreso de mano de sus desarrolladores y promotores.

Todavía estamos tratando de cerrar algunos detalles con el objetivo de dar más facilidades a la participación y asistencia a esta nueva edición.

### Sobre el congreso

Este año el congreso tendrá la opción de realizarse presencialmente en Zaragoza gracias a que además de la participación de las comunidades anfitrionas (representadas inicialmente por **Arturo Martín, Ignacio Sancho y Eduardo Romero**), el **[Ayuntamiento de Zaragoza](https://www.zaragoza.es/)** nos ha cedido diversos espacios en **[Etopia](https://etopia.es/)**, el Centro de Arte y Tecnología de Zaragoza (agradeciendo también a **Elena Giner**, responsable de la gestión de contenidos de dicho centro, por facilitarnos las gestiones).

<p class="logo_alg">
<img height="250" alt="Logo Vitalinux" src="/2023/assets/logos/vitalinux.png">
<img class="logo_pr" height="250" alt="Logo migasfree" src="/2023/assets/logos/migasfree.png">
</p>

Se seguirá el mismo formato que en años anteriores: charlas, talleres, salas de comunidades… contando con el apoyo de asociaciones como **Interferencias**, **LibreLabGRX** o **GALPon** y otros organismos como la **OfiLibre de la URJC**, **AGASOL**, **Wikimedia España** o la **OSLUZ**, que ayudarán a la organización y sumarán esfuerzos en la realización de esta edición en formato híbrido presencial/virtual.

### Cómo llegar

El congreso se realizará en **[Etopia](https://etopia.es/)**, un espacio del **Ayuntamiento de Zaragoza** que alberga laboratorios, espacios de creación, salas de exposiciones, incubadoras de industrias culturales y creativas y una residencia para creadores multidisciplinares, con un equipo humano que favorece la interconexión entre todas ellas. Situado a excasos 10 minutos a pie de la [estación de tren Zaragoza-Delicias](https://www.adif.es/w/04040-zaragoza-delicias) y la [Estación Central de Autobuses de Zaragoza](https://www.estacion-zaragoza.es/) y unos 10 km del [Aeropuerto de Zaragoza](https://www.aena.es/es/zaragoza.html), será fácil llegar a la edición de este año por carretera, tren o avión.

<a title="Ajzh2074, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Etopia_Milla_Digital_Zaragoza_1.jpg"><img style="display: block; margin-left: auto; margin-right: auto;" width="50%" alt="Etopia Milla Digital Zaragoza" src="https://upload.wikimedia.org/wikipedia/commons/d/d9/Etopia_Milla_Digital_Zaragoza_1.jpg"></a>

<a href="https://www.zaragoza.es/" target="_blank"><img src="/2023/assets/logos/ayuntamiento_zaragoza.png" alt="Ayuntamiento de Zaragoza" style="display: block; margin-left: auto; margin-right: auto;" class="images-small"></a>

<iframe style="padding-top: 2em !important" width="100%" height="300px" frameborder="0" allowfullscreen src="https://umap.openstreetmap.fr/es/map/etopia-eslibre_865526?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=2617185#16/41.6603/-0.9030"></iframe><p><a href="https://umap.openstreetmap.fr/es/map/etopia-eslibre_865526#16/41.6600/-0.9042" target="_blank">Ver pantalla completa</a></p>

<p style="text-align: center;">Si tienes cualquier duda, no dudes en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</p>

<h3 style="padding-top: 10px; text-align: center;">¡¡¡NOS VEMOS DENTRO DE POCO!!!<br>👏👏👏</h3>
