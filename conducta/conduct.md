---
layout: 2023/default
section: conducta
permalink: /conduct
title: Code of Conduct in esLibre
---

<h1 class="mt-3">Code of Conduct</h1>

## Purpose

**es<span class="red">Libre</span>** believes in a **community open to everyone**. As such, we are committed to providing a friendly, safe and welcoming environment for everyone regardless of gender, sex, sexual orientation, specific abilities/needs, ethnicity/place of birth or religion (even preferred operating system or programming language and text editor!)

<br>
<p style="text-align: center !important">
<span class="purpose-blockquote">We are looking for people who want to talk about libre culture with empathy.</span>
</p>
<br>

This code of conduct is intended to show our willingness to create a safe and relaxed environment at the event, committing participants, volunteers and speakers to be responsible and respectful.

## Creating community

An additional goal of this code of conduct is to **create a healthy community around libre culture** by encouraging participants to recognise and strengthen the relationships between what we do and **the wider community**.

To accomplish this, the organising team of **es<span class="red">Libre</span>** will collect nominations from exemplary individuals throughout the event and will recognise selected participants after the conference on the website.

If you see someone who is going the extra mile to ensure that our community is welcoming, friendly, and encourages all participants to contribute as much as possible, we want to know about it.

You can nominate someone by commenting to any member of the organising team throughout the congress.

## Thank you for...

-   Be considerate, respectful and cooperative.
-   Refrain from degrading, discriminatory or harassing behaviour.
-   Be aware of your surroundings and your fellow participants. Alert the organising team if you observe a dangerous situation or think someone needs help.
-   Participate in an authentic and active way. By doing so, you are helping to make **es<span class="red">Libre</span>** a reality.
-   Help us share content and reach out to all kinds of people and environments - that's the spirit of **es<span class="red">Libre</span>**!
-   Do not send spam, copyrighted content or generally illegal content through the official channels.

## Please report the following behaviours if you witness them:

-   Intimidating, harassing, abusive, discriminatory, derogatory or degrading actions or words by any attendee or participant of **es<span class="red">Libre</span>** and related events.
-   Spam in event chats and groups, suspicious files, pishing and malware.
-   Disparaging comments and threats related to gender.
-   Transphobic comments, TERFs and similar.
-   Disparaging comments and threats against people of any sexual orientation.
-   Racist comments and threats.
-   Comments and threats against people because of their religion, belief or spirituality, of any kind.
-   Comments and threats that advocate fascism, genocide, supremacism or any political nature that promotes intolerance.
-   Non-consensual and decontextualised use of nudity or abuse of privacy.
-   Harassment of any kind, against anyone or a group of people.
-   The use of non-consensual images of individuals.

The event reserves the right to expand this list in order to protect participants and attendees.

## The **es<span class="red">Libre</span>** organising team takes this issue very seriously, and will take the following measures if necessary:

-   Any alerted behaviour will be quickly dealt with and there will be no exceptions for speakers, organising team, attendees or participants of **es<span class="red">Libre</span>**.
-   The organising team is responsible for creating an appropriate and safe environment, and therefore listens to suggestions and not just warnings.
-   If deemed appropriate, the event reserves the right to _permanently expel_ participants for the aforementioned toxic behaviour.

## What to do if you witness or are subjected to unacceptable behaviour?

If you are involved in unacceptable behaviour, witness another person engaging in such behaviour or have other concerns, please notify the organising team as soon as possible.

Both in the physical editions and in the virtual spaces, in addition to people identified as part of the administration in each room, you will have a point inside the venue to go to if you have any problems.

You can also write to us at <mailto:conducta@eslib.re> or report any problems via the chat that will be set up during the congress for the same purpose. All complaints received will be deleted once the congress is over in order to guarantee their security and anonymity.

**Remember**: The **es<span class="red">Libre</span>** team will be available to assist anyone participating in the congress as local security contacts or to offer any assistance to those experiencing unacceptable behaviour.

## Thank you for helping us to create a safe environment!

We hope for sure that the event will go great, but if something happens we will be there to manage it quickly. We want to create an open and healthy community, so we need your help for that: enjoy the event, share and learn; we will make sure that everyone feels at ease.

## Contact information

If you feel the need to contact us, remember that you can contact us at any time:

-   Email: <mailto:hola@eslib.re>
-   Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:matrix.org)
-   Telegram: [@esLibre](https://t.me/esLibre)
-   Mastodon: [@eslibre@floss.social](https://floss.social/@eslibre/)
-   Twitter: [@esLibre\_](https://twitter.com/esLibre_)
