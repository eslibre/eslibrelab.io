---
layout: 2022/default-gl
section: conducta
permalink: /conduta
title: Código de Conduta en esLibre
---

<h1 class="mt-3">Código de Conduta</h1>

## Propósito

**es<span class="red">Libre</span>** cree nunha **comunidade aberta a todo o mundo**. Como tal, comprometémonos a fornecer un **entorno amigable, seguro e acolledor para todos** independentemente do seu xénero, sexo, orientación sexual, capacidades/necesidades específicas, etnia ou lugar de nacemento, relixión (incluso sistema operativo preferido ou linguaxe de programación e editor de texto!)

Buscamos **persoas** que queiran falar de cultura libre con empatía.

Este código de conduta pretende amosar a nosa disposición para crear un entorno seguro e distender no evento, comprometendo as persoas participantes, voluntarias e relatoras a ser responsables e respectuosas.

## Crear comunidade

Un obxectivo adicional deste código de conduta é **crear unha comunidade sa arredor da cultura libre** afoutando aos participantes a recoñecer e fortalecer as relacións entre o que facemos e **a comunidade** en xeral.

Para cumprir isto, os organizadores de **es<span class="rede">Libre</span>** recollerán candidaturas de persoas exemplares ao longo de todo o evento e recoñecerán aos participantes seleccionados após o congreso no sitio web.

Se ves a alguén que está a facer un esforzo extra para asegurar que a nosa comunidade resulte acolledora, amable, e que anime a todos os participantes a colaborar na maior medida posible, querémolo saber.

Podes nomear a alguén comentándollo a calquera membro da organización durante todo o congreso.

## Grazas por...

-   Ser considerado, respectuoso e colaborativo.
-   Absterse de comportamentos degradantes, discriminatorios ou acosadores.
-   Ser consciente do entorno e dos teus compañeiros participantes. Avisa á organización se ves unha situación perigosa ou pensas que alguén necesita axuda.
-   Participar dun xeito auténtico e activo. Ao facelo, estás a axudar a que **es<span class="red">Libre</span>** sexa unha realidade.
-   Axúdanos a compartir contido e chegar a todo tipo de persoas e entornos, ese é o espírito de **es<span class="red">Libre</span>**!
-   Non envíes lixo, contido protexido por copyright ou contido ilegal en xeral a través das canles oficiais.

## Avisa se es testemuña dos seguintes comportamentos:

-   Accións ou palabras intimidantes, acosadoras, abusivas, discriminatorias, despectivas ou degradantes por parte de calquera asistente ou participante de **es<span class="red">Libre</span>** ou eventos relacionados.
-   Lixo en conversas e grupos de eventos, ficheiros sospeitosos, phishing e malware.
-   Comentarios despectivos e ameazas relacionados co xénero.
-   Comentarios transfóbicos, TERF e similares.
-   Comentarios despectivos e ameazas contra persoas de calquera orientación sexual.
-   Comentarios e ameazas racistas.
-   Comentarios e ameazas contra persoas pola súa relixión, crenza ou espiritualidade, de calquera tipo.
-   Comentarios e ameazas que propugnan o fascismo, o xenocidio, o supremacismo ou de calquera natureza política que fomente a intolerancia.
-   Uso non consentido e descontextualizado do espido ou abuso da privacidade.
-   Acoso de calquera tipo, contra alguén ou un grupo de persoas.
-   O uso de imaxes non consensuadas de persoas.

O evento resérvase o dereito de ampliar esta lista co fin de protexer ás persoas que participan e asisten ao congreso.

## A organización **es<span class="red">Libre</span>** tómase moi en serio esta cuestión e adoptará as seguintes medidas se é necesario:

-   Calquera comportamento alertado xestionarase rapidamente e non haberá excepcións para os relatores, organización, asistentes ou participantes de **es<span class="red">Libre</span>**.
-   A organización encárgase de crear un entorno axeitado e seguro, polo que escoita as suxestións e non só as advertencias.
-   Se se considera oportuno, o evento resérvase o dereito de _expulsar definitivamente_ aos participantes nas condutas tóxicas mencionadas anteriormente.

## Que facer se es testemuña ou obxecto dun comportamento inaceptable?

Se estás implicado nun comportamento inaceptable, presencias que outra persoa participa en tal comportamento ou tes outras dúbidas, avisa á organización do evento o máis axiña posible.

Tanto nas edicións físicas como nos espazos virtuais, ademais das persoas identificadas como parte da administración en cada sala, terás un punto dentro do recinto ao que acudir se tes algún problema.

Tamén podes escribirnos a [conducta@eslib.re](mailto:conducta@eslib.re) ou comunicarnos calquera problema a través da canle de conversa que se activará durante o congreso con este mesmo obxectivo. Toda denuncia que se reciba será eliminada unha vez rematado o congreso para garantir a seguridade e anonimato das mesmas.

**Lembra**: o equipo de **es<span class="red">Libre</span>** estará dispoñible para axudar a quen participe no congreso como contacto de seguridade local ou para ofrecer calquera tipo de axuda a aqueles que experimenten un comportamento inaceptable.

## Grazas por axudarnos a crear un entorno seguro!

Agardamos que o evento saia moi ben, mais se acontece algo estaremos alí para xestionalo rapidamente. Queremos crear unha comunidade aberta e saudable, polo que necesitamos a túa axuda para iso: gozar do evento, compartir e aprender; dende a organización procuraremos que todos se sintan cómodos.

## Información de contacto

Se te atopas na necesidade de contactar connosco, lembra que podes contactar connosco en calquera momento a través de:

*   Correo electrónico: [hola@eslib.re](mailto:hola@eslib.re)
*   Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:matrix.org)
*   Telegram: [@esLibre](https://t.me/esLibre)
*   Mastodon: [@eslibre@floss.social](https://floss.social/@eslibre/)
*   Twitter: [@esLibre_](https://twitter.com/esLibre_)
